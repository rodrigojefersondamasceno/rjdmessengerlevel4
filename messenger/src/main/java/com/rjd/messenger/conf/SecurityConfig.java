package com.rjd.messenger.conf;

import com.rjd.messenger.enuns.UserRole;
import com.rjd.messenger.service.MessengerUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import java.util.Arrays;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    MessengerUserDetailService userDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/messenger/**").permitAll()
                .antMatchers("/message/audit", "/message/audit/**").hasRole(UserRole.AUDITOR.toString())
                .antMatchers("/message", "/message/**").hasAnyRole(UserRole.ADMIN.toString(), UserRole.COMUM.toString(), UserRole.AUDITOR.toString())
                .antMatchers( "/user/filter","/user/filter/**").hasAnyRole(UserRole.ADMIN.toString(), UserRole.COMUM.toString(), UserRole.AUDITOR.toString())
                .antMatchers( HttpMethod.GET, "/user","/user/**").hasAnyRole(UserRole.ADMIN.toString(), UserRole.COMUM.toString(), UserRole.AUDITOR.toString())
                .antMatchers( "/user","/user/**").hasRole(UserRole.ADMIN.toString())
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().formLogin().permitAll()
                .and().logout().invalidateHttpSession(true).deleteCookies("JSESSIONID").logoutUrl("/logout");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.eraseCredentials(false).userDetailsService(userDetailService).passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


}
