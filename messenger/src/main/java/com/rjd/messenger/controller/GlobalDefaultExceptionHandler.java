package com.rjd.messenger.controller;

import com.mongodb.MongoSocketOpenException;
import com.rjd.messenger.component.MyMessageSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.ConnectException;

@ControllerAdvice
public class GlobalDefaultExceptionHandler extends ResponseEntityExceptionHandler {

    private Logger logger = Logger.getLogger(this.getClass());

    private MyMessageSource myMessageSource;

    @Autowired
    public GlobalDefaultExceptionHandler(MyMessageSource myMessageSource) {
        this.myMessageSource = myMessageSource;
    }

    @ExceptionHandler({DataAccessException.class, ConnectException.class, MongoSocketOpenException.class})
    public ResponseEntity<Object> dataAccessExcptionHandler(Exception e, WebRequest req) {
        logger.error(e.getMessage(),e);

        return handleExceptionInternal(e,myMessageSource.get("msg.error.database.connection"), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, req);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> defaultErrorHandler( Exception e, WebRequest req) throws Exception{

        if(AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null){
            throw e;
        }

        return handleExceptionInternal(e, myMessageSource.get("msg.error.unexpected"), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, req);
    }
}
