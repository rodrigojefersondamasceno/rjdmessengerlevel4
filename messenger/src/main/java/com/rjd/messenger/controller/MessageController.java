package com.rjd.messenger.controller;

import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.service.message.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(path="/message")
public class MessageController {


    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("")
    public MessageDTO create(@RequestBody MessageDTO messageDTO){
        return messageService.create(messageDTO);
    }

    @GetMapping("/{id}")
    public MessageDTO findById(@PathVariable("id") String id){
        return messageService.findById(id);
    }

    @PostMapping("/chat")
    public List<MessageDTO> findChat(@RequestBody FiltroMessageDTO filtroMessageDTO) {
        return messageService.findChat(filtroMessageDTO);
    }

    @PostMapping("/audit")
    public PagMessageDTO findAllMessagesForAudit(@RequestBody FiltroMessageDTO filtroMessageDTO) {
        return messageService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @PutMapping("")
    public MessageDTO update(@RequestBody MessageDTO messageDTO){
        return messageService.update(messageDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") String id){
        messageService.deleteById(id);
    }
}
