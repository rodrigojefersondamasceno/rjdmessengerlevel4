package com.rjd.messenger.controller;

import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class MessengerWebSocketController {

    @MessageMapping("/sendMessage")
    @SendToUser("/queue/listen")
    public MessageDTO processMessageFromClient(@Payload MessageDTO messageDto, Principal principal){
        return messageDto;
    }

    @MessageExceptionHandler
    @SendToUser(value = "/queue/errors", broadcast = false)
    public String handleException(Throwable exception){
        return exception.getMessage();
    }

}
