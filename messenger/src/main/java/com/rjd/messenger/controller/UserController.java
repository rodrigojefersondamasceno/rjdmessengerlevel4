package com.rjd.messenger.controller;

import com.rjd.messenger.dto.FiltroUserDTO;
import com.rjd.messenger.dto.PagUserDTO;
import com.rjd.messenger.entity.User;
import com.rjd.messenger.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable("id") String id){
        return userService.findById(id);
    }

    @GetMapping("")
    public PagUserDTO findAll(@RequestParam("nuPage") String nuPage, @RequestParam("pageSize") String pageSize) {
        return userService.findAll(nuPage, pageSize);
    }

    @PostMapping("/filter")
    public PagUserDTO findAll(@RequestBody FiltroUserDTO filtro) {
        return userService.findAll(filtro);
    }

    @PostMapping("")
    public User create(@RequestBody User user){
        return userService.create(user);
    }

    @PutMapping("")
    public User update(@RequestBody User user){
        return userService.update(user);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") String id){
        userService.deleteById(id);
    }
}
