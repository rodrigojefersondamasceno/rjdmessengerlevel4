package com.rjd.messenger.dto;

import java.io.Serializable;
import java.util.Objects;

public class FiltroMessageDTO implements Serializable {
    private String senderId;
    private String receiverId;
    private String nuPage;
    private String pageSize;

    public FiltroMessageDTO() {
    }

    public FiltroMessageDTO(String senderId, String receiverId) {
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public FiltroMessageDTO(String senderId, String receiverId, String nuPage, String pageSize) {
        this(senderId, receiverId);
        this.nuPage = nuPage;
        this.pageSize = pageSize;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getNuPage() {
        return nuPage;
    }

    public void setNuPage(String nuPage) {
        this.nuPage = nuPage;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FiltroMessageDTO that = (FiltroMessageDTO) o;
        return Objects.equals(senderId, that.senderId) &&
                Objects.equals(receiverId, that.receiverId) &&
                Objects.equals(nuPage, that.nuPage) &&
                Objects.equals(pageSize, that.pageSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(senderId, receiverId, nuPage, pageSize);
    }
}
