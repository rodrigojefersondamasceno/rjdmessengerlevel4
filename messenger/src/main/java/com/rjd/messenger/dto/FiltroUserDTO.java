package com.rjd.messenger.dto;

import java.io.Serializable;
import java.util.Objects;

public class FiltroUserDTO implements Serializable {
    private String name;
    private String lastName;
    private String nuPage;
    private String pageSize;

    public FiltroUserDTO() {
    }

    public FiltroUserDTO(String name, String lastName, String nuPage, String pageSize) {
        this.name = name;
        this.lastName = lastName;
        this.nuPage = nuPage;
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNuPage() {
        return nuPage;
    }

    public void setNuPage(String nuPage) {
        this.nuPage = nuPage;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FiltroUserDTO that = (FiltroUserDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(nuPage, that.nuPage) &&
                Objects.equals(pageSize, that.pageSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, nuPage, pageSize);
    }
}
