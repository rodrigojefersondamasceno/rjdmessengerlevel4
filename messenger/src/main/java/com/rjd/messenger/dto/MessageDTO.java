package com.rjd.messenger.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class MessageDTO implements Serializable {

    private String id;

    private String senderId;

    private String senderName;

    private String receiverId;

    private String receiverName;

    private String message;

    private String dtMessageLongStr;

    public MessageDTO(){
    }

    public MessageDTO(String senderId, String senderName, String receiverId, String receiverName, String message) {
        this();
        this.senderId = senderId;
        this.senderName = senderName;
        this.receiverId = receiverId;
        this.receiverName = receiverName;
        this.message = message;
        this.dtMessageLongStr = Long.valueOf(new Date().getTime()).toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDtMessageLongStr() {
        return dtMessageLongStr;
    }

    public void setDtMessageLongStr(String dtMessageLongStr) {
        this.dtMessageLongStr = dtMessageLongStr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDTO that = (MessageDTO) o;
        return Objects.equals(senderId, that.senderId) &&
                Objects.equals(receiverId, that.receiverId) &&
                Objects.equals(message, that.message) &&
                Objects.equals(dtMessageLongStr, that.dtMessageLongStr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(senderId, receiverId, message, dtMessageLongStr);
    }
}
