package com.rjd.messenger.dto;

import com.rjd.messenger.entity.ChatRoom;

import java.util.ArrayList;
import java.util.List;

public class PagChatRoomDTO {
    private int nuPageAtual;
    private int nuPageTotal;
    private List<ChatRoom> chatRoomList;

    public PagChatRoomDTO() {
        chatRoomList = new ArrayList<>();
    }

    public PagChatRoomDTO(int nuPageAtual, int nuPageTotal, List<ChatRoom> chatRoomList) {
        this.nuPageAtual = nuPageAtual;
        this.nuPageTotal = nuPageTotal;
        this.chatRoomList = chatRoomList;
    }

    public int getNuPageAtual() {
        return nuPageAtual;
    }

    public void setNuPageAtual(int nuPageAtual) {
        this.nuPageAtual = nuPageAtual;
    }

    public int getNuPageTotal() {
        return nuPageTotal;
    }

    public void setNuPageTotal(int nuPageTotal) {
        this.nuPageTotal = nuPageTotal;
    }

    public List<ChatRoom> getChatRoomList() {
        return chatRoomList;
    }

    public void setChatRoomList(List<ChatRoom> chatRoomList) {
        this.chatRoomList = chatRoomList;
    }
}
