package com.rjd.messenger.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PagMessageDTO implements Serializable {
    private int nuPageAtual;
    private int nuPageTotal;
    List<MessageDTO> messageDTOList;

    public PagMessageDTO(int nuPageAtual, int nuPageTotal, List<MessageDTO> messageDTOList) {
        this.nuPageAtual = nuPageAtual;
        this.nuPageTotal = nuPageTotal;
        this.messageDTOList = messageDTOList;
    }

    public PagMessageDTO() {
        messageDTOList = new ArrayList<>();
    }

    public int getNuPageAtual() {
        return nuPageAtual;
    }

    public void setNuPageAtual(int nuPageAtual) {
        this.nuPageAtual = nuPageAtual;
    }

    public int getNuPageTotal() {
        return nuPageTotal;
    }

    public void setNuPageTotal(int nuPageTotal) {
        this.nuPageTotal = nuPageTotal;
    }

    public List<MessageDTO> getMessageDTOList() {
        return messageDTOList;
    }

    public void setMessageDTOList(List<MessageDTO> messageDTOList) {
        this.messageDTOList = messageDTOList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PagMessageDTO that = (PagMessageDTO) o;
        return nuPageAtual == that.nuPageAtual &&
                nuPageTotal == that.nuPageTotal &&
                Objects.equals(messageDTOList, that.messageDTOList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nuPageAtual, nuPageTotal, messageDTOList);
    }
}
