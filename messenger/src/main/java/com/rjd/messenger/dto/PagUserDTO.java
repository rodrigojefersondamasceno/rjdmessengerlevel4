package com.rjd.messenger.dto;

import com.rjd.messenger.entity.User;
import org.apache.avro.generic.GenericData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PagUserDTO implements Serializable {

    private int nuPageAtual;
    private int nuPageTotal;
    private List<User> usersList;

    public PagUserDTO() {
        this.usersList = new ArrayList<>();
    }

    public PagUserDTO(int nuPageAtual, int nuPageTotal, List<User> usersList) {
        this();
        this.nuPageAtual = nuPageAtual;
        this.nuPageTotal = nuPageTotal;
        this.usersList = usersList;
    }

    public int getNuPageAtual() {
        return nuPageAtual;
    }

    public void setNuPageAtual(int nuPageAtual) {
        this.nuPageAtual = nuPageAtual;
    }

    public int getNuPageTotal() {
        return nuPageTotal;
    }

    public void setNuPageTotal(int nuPageTotal) {
        this.nuPageTotal = nuPageTotal;
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PagUserDTO that = (PagUserDTO) o;
        return nuPageAtual == that.nuPageAtual &&
                nuPageTotal == that.nuPageTotal &&
                Objects.equals(usersList, that.usersList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nuPageAtual, nuPageTotal, usersList);
    }
}
