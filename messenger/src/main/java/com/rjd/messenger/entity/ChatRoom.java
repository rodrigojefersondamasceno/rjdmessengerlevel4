package com.rjd.messenger.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Document
public class ChatRoom {
    @Id
    private String id;

    private String name;

    private List<String> usersId;

    public ChatRoom() {
    }

    public ChatRoom(String id, String name, List<String> usersId) {
        this.id = id;
        this.name = name;
        this.usersId = usersId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUsersId() {
        return usersId;
    }

    public void setUsersId(List<String> usersId) {
        this.usersId = usersId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatRoom chatRoom = (ChatRoom) o;
        return id.equals(chatRoom.id) &&
                name.equals(chatRoom.name) &&
                usersId.equals(chatRoom.usersId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, usersId);
    }
}
