package com.rjd.messenger.entity;

import org.apache.hadoop.hbase.util.Bytes;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.util.Objects;

@Document(indexName = "rdamasceno", type = "messages")
public class Message implements Comparable<Message>{

    @Id
    private String id;

    private String senderId;

    private String receiverId;

    private String message;

    private long dtMessage;

    public static final String MESSAGE_TABLE_NAME = Message.class.getSimpleName();
    public static final byte[] tableNameAsBytes = Bytes.toBytes(MESSAGE_TABLE_NAME);
    public static final String columnFamillyMessage = "CF_MESSAGE";
    public static final byte[] columnFamillyMessageAsBytes = Bytes.toBytes(columnFamillyMessage);
    public static final byte[] idAsBytes = Bytes.toBytes("id");
    public static final byte[] senderIdAsBytes = Bytes.toBytes("senderId");
    public static final byte[] receiverIdAsBytes = Bytes.toBytes("receiverId");
    public static final byte[] messageAsBytes = Bytes.toBytes("message");
    public static final byte[] dtMessageAsBytes = Bytes.toBytes( "dtMessage");

    public static final Message bytesToMessage(byte[] id, byte[] senderId, byte[] receiverId, byte[] message, long dtMessage){
        return new Message(Bytes.toString(id), Bytes.toString(senderId), Bytes.toString(receiverId), Bytes.toString(message), dtMessage);
    }

    public Message(){
    }

    public Message(String senderId, String receiverId, String message) {
        this();
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
    }

    public Message(String id, String senderId, String receiverId, String message, long dtMessage) {
        this(senderId, receiverId, message);
        this.id = id;
        this.dtMessage = dtMessage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDtMessage() {
        return dtMessage;
    }

    public void setDtMessage(long dtMessage) {
        this.dtMessage = dtMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message1 = (Message) o;
        return Objects.equals(getSenderId(), message1.getSenderId()) &&
                Objects.equals(getReceiverId(), message1.getReceiverId()) &&
                Objects.equals(getMessage(), message1.getMessage()) &&
                Objects.equals(getDtMessage(), message1.getDtMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSenderId(), getReceiverId(), getMessage(), getDtMessage());
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", sender='" + senderId + '\'' +
                ", receiver='" + receiverId + '\'' +
                ", message='" + message + '\'' +
                ", dtMessage=" + dtMessage +
                '}';
    }

    @Override
    public int compareTo(Message o) {
        return o.getDtMessage() < dtMessage ? 1 : -1;
    }
}
