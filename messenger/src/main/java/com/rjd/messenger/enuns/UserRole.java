package com.rjd.messenger.enuns;

public enum UserRole {

    ADMIN, COMUM, AUDITOR;

    public static UserRole fromString(String roleStrParam){
        for(UserRole role: UserRole.values()){
            if(role.toString().equalsIgnoreCase(roleStrParam)){
                return role;
            }
        }
        return null;
    }
}
