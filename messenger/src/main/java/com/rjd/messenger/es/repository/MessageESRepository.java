package com.rjd.messenger.es.repository;

import com.rjd.messenger.entity.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageESRepository extends ElasticsearchRepository<Message, String> {

    Iterable<Message> findBySenderIdAndReceiverId(String senderId, String receiverId);

    Page<Message> findBySenderId(String senderId, Pageable page);

    Page<Message> findByReceiverId(String receiverId, Pageable page);

    Page<Message> findBySenderIdAndReceiverId(String senderId, String receiverId, Pageable page);
}
