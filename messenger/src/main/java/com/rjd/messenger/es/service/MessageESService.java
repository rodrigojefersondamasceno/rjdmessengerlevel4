package com.rjd.messenger.es.service;

import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;

import java.util.List;

public interface MessageESService {
    Message create(Message message);

    Message findById(String id);

    Message update(Message message);

    void delete(String id);

    List<Message> findAll();

    List<Message> findMessagesFromChat(FiltroMessageDTO filtroMessageDTO);

    PagMessageDTO findAllMessagesForAudit(FiltroMessageDTO filtroMessageDTO);
}
