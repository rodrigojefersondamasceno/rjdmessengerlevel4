package com.rjd.messenger.es.service;

import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.es.repository.MessageESRepository;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.service.message.MessageAbstractService;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.util.CollectionUtil;
import com.rjd.messenger.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class MessageESServiceImpl extends MessageAbstractService implements MessageESService{

    private MessageESRepository messageESRepository;

    @Autowired
    public MessageESServiceImpl(MessageESRepository messageESRepository, UserService userService, MyMessageSource messageSource) {
        this.messageESRepository = messageESRepository;
        this.myMessageSource = messageSource;
        this.userService = userService;
    }

    @Override
    public Message create(Message message) {
        try {
            return messageESRepository.save(message);
        }catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public Message findById(String id) {
        try {
            return messageESRepository.findById(id).get();
        }catch(NoSuchElementException e){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.user.not.found") + " " + id);
        }
    }

    @Override
    public Message update(Message message) {
        try {
            return messageESRepository.save(message);
        }catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void delete(String id) {
        messageESRepository.deleteById(id);
    }

    @Override
    public List<Message> findAll() {
        return CollectionUtil.toList(messageESRepository.findAll());
    }

    @Override
    public List<Message> findMessagesFromChat(FiltroMessageDTO filtroMessageDTO) {
        Iterable<Message> messagesSentIt = messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId());
        Iterable<Message> messagesReceivedIt = messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId());
        List<Message> messagesSentList = CollectionUtil.toList(messagesSentIt);
        List<Message> messagesReceivedList = CollectionUtil.toList(messagesReceivedIt);
        if(messagesSentList.isEmpty() && messagesReceivedList.isEmpty()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.messages.not.found"));
        }
        List<Message> messagesTraded = new ArrayList<>();
        messagesTraded.addAll(messagesSentList);
        messagesTraded.addAll(messagesReceivedList);
        return messagesTraded;
    }

    @Override
    public PagMessageDTO findAllMessagesForAudit(FiltroMessageDTO filtroMessageDTO) {
        StringUtils.validateIfStringIsInteger(filtroMessageDTO.getNuPage(), "nuPage");
        StringUtils.validateIfStringIsInteger(filtroMessageDTO.getPageSize(), "pageSize");
        Page<Message> messagesPage = null;
        if(StringUtils.isNotNullOrBlank(filtroMessageDTO.getSenderId()) && StringUtils.isNullOrBlank(filtroMessageDTO.getReceiverId())){
            messagesPage = messageESRepository.findBySenderId(filtroMessageDTO.getSenderId(), PageRequest.of(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize())));
        }else if(StringUtils.isNullOrBlank(filtroMessageDTO.getSenderId()) && StringUtils.isNotNullOrBlank(filtroMessageDTO.getReceiverId())){
            messagesPage = messageESRepository.findByReceiverId(filtroMessageDTO.getReceiverId(), PageRequest.of(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize())));
        }else if(StringUtils.isNotNullOrBlank(filtroMessageDTO.getSenderId()) && StringUtils.isNotNullOrBlank(filtroMessageDTO.getReceiverId())){
            messagesPage = messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId(), PageRequest.of(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize())));
        }else{
            messagesPage = messageESRepository.findAll(PageRequest.of(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize())));
        };
        if(messagesPage.getContent().isEmpty()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.messages.not.found"));
        }
        List<Message> messageList = new ArrayList<>(messagesPage.getContent());
        List<MessageDTO> messageDTOList = convertMessageListToMessageDTOListSort(messageList);
        return new PagMessageDTO(messagesPage.getNumber(), messagesPage.getTotalPages(), messageDTOList);
    }
}
