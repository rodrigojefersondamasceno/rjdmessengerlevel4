package com.rjd.messenger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EntityAlreadyRegisteredException extends RuntimeException {
    public EntityAlreadyRegisteredException() {
        super();
    }

    public EntityAlreadyRegisteredException(String message) {
        super(message);
    }

    public EntityAlreadyRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityAlreadyRegisteredException(Throwable cause) {
        super(cause);
    }

    public EntityAlreadyRegisteredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
