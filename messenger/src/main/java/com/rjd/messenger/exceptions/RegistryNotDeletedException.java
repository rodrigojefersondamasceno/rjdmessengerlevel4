package com.rjd.messenger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class RegistryNotDeletedException extends RuntimeException {
    public RegistryNotDeletedException() {
        super();
    }

    public RegistryNotDeletedException(String message) {
        super(message);
    }

    public RegistryNotDeletedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegistryNotDeletedException(Throwable cause) {
        super(cause);
    }

    public RegistryNotDeletedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
