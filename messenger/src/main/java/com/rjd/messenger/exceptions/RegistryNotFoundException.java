package com.rjd.messenger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RegistryNotFoundException extends RuntimeException{
    public RegistryNotFoundException() {
        super();
    }

    public RegistryNotFoundException(String message) {
        super(message);
    }

    public RegistryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegistryNotFoundException(Throwable cause) {
        super(cause);
    }

    public RegistryNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
