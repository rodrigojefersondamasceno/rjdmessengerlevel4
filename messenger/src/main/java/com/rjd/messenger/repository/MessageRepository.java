package com.rjd.messenger.repository;

import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.util.StringUtils;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MessageRepository {

    private Logger LOGGER = LoggerFactory.getLogger(MessageRepository.class);

    private HbaseTemplate hbaseTemplate;

    private MyMessageSource myMessageSource;

    @Autowired
    public MessageRepository(HbaseTemplate hbaseTemplate, MyMessageSource myMessageSource) {
        this.hbaseTemplate = hbaseTemplate;
        this.myMessageSource = myMessageSource;
    }

    public Message create(Message message) {
        LOGGER.info("Called createMessage {}", message);
        save(message);
        return findById(message.getId());
    }

    public Message findById(String id){
        LOGGER.info("Called findMesageById method {}", id);
        SingleColumnValueFilter singleColumnValueFilter = createFilterEqual(Message.idAsBytes, id);

        Scan scan = createScan(singleColumnValueFilter);
        Message message = findByScan(scan, id);

        return message;
    }

    public Message update(Message message) {
        LOGGER.info("Called updateMessage {}", message);
        save(message);
        return findById(message.getId());
    }

    public void delete(String id) {
        LOGGER.info("Called deleteMessage {}", id);
        Delete delete = new Delete(Bytes.toBytes(id));
        hbaseTemplate.execute(Message.MESSAGE_TABLE_NAME, hTableInterface -> {
            hTableInterface.delete(delete);
            return null;
        });
    }

    public List<Message> findAll() {
        LOGGER.info("Called findAll");
        return hbaseTemplate.find(Message.MESSAGE_TABLE_NAME, Message.columnFamillyMessage, (result, i) ->
                getMessageFromResult(result)
        );
    }

    public List<Message> findMessagesFromChat(FiltroMessageDTO filtroMessageDTO) {
        LOGGER.info("Called findMessagesTradedBetweenSenderAndReceiver");
        SingleColumnValueFilter filtroSenderIsSender = createFilterEqual(Message.senderIdAsBytes, filtroMessageDTO.getSenderId());
        SingleColumnValueFilter filtroReceiverIsReceiver = createFilterEqual(Message.receiverIdAsBytes, filtroMessageDTO.getReceiverId());

        //filtro de mensagmes enviada por sender e recebidas por receiver
        FilterList filterListMessagesSent = new FilterList(FilterList.Operator.MUST_PASS_ALL, filtroSenderIsSender, filtroReceiverIsReceiver);

        SingleColumnValueFilter filtroSenderIsReceiver = createFilterEqual(Message.senderIdAsBytes, filtroMessageDTO.getReceiverId());
        SingleColumnValueFilter filtroReceiverIsSender = createFilterEqual(Message.receiverIdAsBytes, filtroMessageDTO.getSenderId());

        FilterList filterListMessagesReceived = new FilterList(FilterList.Operator.MUST_PASS_ALL, filtroSenderIsReceiver, filtroReceiverIsSender);

        FilterList filterListMessagesSentAndReceived = new FilterList(FilterList.Operator.MUST_PASS_ONE, filterListMessagesSent, filterListMessagesReceived);

        Scan scan = createScan(filterListMessagesSentAndReceived);

        return findAllByScan(scan);
    }

    public List<Message> findAllMessagesForAudit(FiltroMessageDTO filtroMessageDTO) {
        SingleColumnValueFilter filtroSender = null;
        SingleColumnValueFilter filtroReceiver = null;

        if(StringUtils.isNotNullOrBlank(filtroMessageDTO.getSenderId())){
            filtroSender = createFilterEqual(Message.senderIdAsBytes, filtroMessageDTO.getSenderId());
        }
        if(StringUtils.isNotNullOrBlank(filtroMessageDTO.getReceiverId())){
            filtroReceiver = createFilterEqual(Message.receiverIdAsBytes, filtroMessageDTO.getReceiverId());
        }

        FilterList filterListMessagesSent = new FilterList(FilterList.Operator.MUST_PASS_ALL);

        if(filtroSender != null){
            filterListMessagesSent.addFilter(filtroSender);
        }

        if(filtroReceiver != null){
            filterListMessagesSent.addFilter(filtroReceiver);
        }

        Scan scan = createScan();

        if(!filterListMessagesSent.getFilters().isEmpty()){
            scan = createScan(filterListMessagesSent);
        }

        return findAllByScan(scan);
    }

    private Message getMessageFromResult(Result result) {
        return Message.bytesToMessage(
                result.getValue(Message.columnFamillyMessageAsBytes, Message.idAsBytes),
                result.getValue(Message.columnFamillyMessageAsBytes, Message.senderIdAsBytes),
                result.getValue(Message.columnFamillyMessageAsBytes, Message.receiverIdAsBytes),
                result.getValue(Message.columnFamillyMessageAsBytes, Message.messageAsBytes),
                result.raw()[0].getTimestamp()
        );
    }

    private Scan createScan(Filter filter){
        Scan scan = createScan();
        scan.setFilter(filter);
        return scan;
    }

    private Scan createScan(){
        Scan scan = new Scan();
        scan.addFamily(Message.columnFamillyMessageAsBytes);
        return scan;
    }

    private SingleColumnValueFilter createFilterEqual(byte[] columAsBytes, String id){
        return new SingleColumnValueFilter(Message.columnFamillyMessageAsBytes, columAsBytes, CompareFilter.CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(id)));
    }

    private void save(Message message) {
        Put put = new Put(Bytes.toBytes(message.getId()));
        populateMessageOnPut(message, put);
        saveMessage(put);
    }

    private void saveMessage(Put put) {
        hbaseTemplate.execute(Message.MESSAGE_TABLE_NAME, hTableInterface -> {
            hTableInterface.put(put);
            return null;
        });
    }

    private void populateMessageOnPut(Message message, Put put) {
        put.add(Message.columnFamillyMessageAsBytes, Message.idAsBytes, Bytes.toBytes(message.getId()));
        put.add(Message.columnFamillyMessageAsBytes, Message.senderIdAsBytes, Bytes.toBytes(message.getSenderId()));
        put.add(Message.columnFamillyMessageAsBytes, Message.receiverIdAsBytes, Bytes.toBytes(message.getReceiverId()));
        put.add(Message.columnFamillyMessageAsBytes, Message.messageAsBytes, Bytes.toBytes(message.getMessage()));
    }

    private Message findByScan(Scan scan, String id){
        List<Message> results = findAllByScan(scan);
        if(results.isEmpty()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.message.not.found") + " " + id);
        }
        return results.get(0);
    }

    private List<Message> findAllByScan(Scan scan) {
        List<Message> messagesList = hbaseTemplate.find(Message.MESSAGE_TABLE_NAME, scan, (result, i) ->
                getMessageFromResult(result)
        );
        if(messagesList.isEmpty()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.messages.not.found"));
        }
        return messagesList;
    }
}
