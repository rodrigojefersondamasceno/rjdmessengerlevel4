package com.rjd.messenger.repository;

import com.rjd.messenger.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {
    Iterable<User> findByIdNot(String id);

    Page<User> findByIdNot(String currentPrincipalName, Pageable page);

    Page<User> findByIdNotAndNameLike(String currentPrincipalName, String firstName, Pageable page);

    Page<User> findByIdNotAndLastNameLike(String currentPrincipalName, String lastName, Pageable page);

    Page<User> findByIdNotAndNameLikeAndLastNameLike(String currentPrincipalName, String firstName, String lastName, Pageable page);
}
