package com.rjd.messenger.service;

import com.rjd.messenger.entity.User;
import com.rjd.messenger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;


@Service
public class MessengerUserDetailService implements UserDetailsService {


    @Autowired
    private UserRepository userRepository;

    private static final String PREFIXO_ROLE = "ROLE_";

    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findById(id);
        if(!userOptional.isPresent()){
            throw new UsernameNotFoundException(id);
        }
        User user = userOptional.get();
        GrantedAuthority authority = new SimpleGrantedAuthority(PREFIXO_ROLE + user.getUserRole());
        return new org.springframework.security.core.userdetails.User(user.getId(), user.getPassword(), Arrays.asList(authority));
    }
}
