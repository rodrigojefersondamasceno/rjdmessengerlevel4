package com.rjd.messenger.service.message;

import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.exceptions.MandatoryFieldException;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.util.MessageUtil;
import com.rjd.messenger.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class MessageAbstractService {

    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    protected MyMessageSource myMessageSource;

    protected UserService userService;

    protected List<MessageDTO> convertMessageListToMessageDTOListSort(List<Message> messages) {
        try {
            if (messages.size() > 1) {
                Collections.sort(messages, Message::compareTo);
            }
            List<MessageDTO> messageDTOList = new ArrayList<>();
            for (Message message : messages) {
                MessageDTO messageDTO = convertMessageToMessageDTO(message);
                messageDTOList.add(messageDTO);
            }
            return messageDTOList;
        }catch(Exception e){
            LOGGER.error(e.getMessage(),e);
            throw e;
        }
    }


    protected PagMessageDTO convertMessageListToMessageDTOListSortPaginated(List<Message> messagesList, FiltroMessageDTO filtroMessageDTO) {
        Collections.sort(messagesList, Message::compareTo);
        if(messagesList.isEmpty()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.messages.not.found"));
        }
        int nuPage = Integer.valueOf(filtroMessageDTO.getNuPage());
        int pageSize = Integer.valueOf(filtroMessageDTO.getPageSize());
        int initPage = nuPage * (pageSize);
        int endPage = (nuPage + 1) * (pageSize);
        if(initPage > messagesList.size()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.messages.not.found"));
        }
        if(endPage >= messagesList.size()){
            endPage = messagesList.size();
        }
        int totalPages = Math.round(messagesList.size() / Float.valueOf(pageSize));
        List<Message> messageListSorted = messagesList.subList(initPage, endPage);
        List<MessageDTO> messageListDTOSorted = convertMessageListToMessageDTOListSort(messageListSorted );
        return new PagMessageDTO(nuPage + 1, totalPages, messageListDTOSorted);
    }



    protected void validateReciverIdSenderIdNotNull(String senderId, String receiverId) {
        if(StringUtils.isNullOrBlank(senderId)){
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.sender.mandatory"));
        }
        if(StringUtils.isNullOrBlank(receiverId)){
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.receiver.mandatory"));
        }
    }

    protected MessageDTO convertMessageToMessageDTO(Message message) {
        MessageDTO messageDTO = MessageUtil.convertMessageToMessageDTO(message);
        messageDTO.setSenderName(userService.findUserNameSafeSilenced(message.getSenderId()));
        messageDTO.setReceiverName(userService.findUserNameSafeSilenced(message.getReceiverId()));
        return messageDTO;
    }



    protected void validateMessageId(String id) {
        if(StringUtils.isNullOrBlank(id)){
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.id.mandatory"));
        }
    }

    protected void validateMessage(Message message) {
        validateReciverIdSenderIdNotNull(message.getSenderId(), message.getReceiverId());
        if(StringUtils.isNullOrBlank(message.getMessage())){
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.message.mandatory"));
        }
        validateIfSenderAndReceiverExists(message.getSenderId(), message.getReceiverId());
    }

    protected void validateIfSenderAndReceiverExists(String senderId, String receiverId) {
        //se sender ou receiver não estiverem cadastrados na base sera lançada exceção.
        userService.findById(senderId);
        userService.findById(receiverId);
    }
}
