package com.rjd.messenger.service.message;

import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;

import java.util.List;

public interface MessageService {
    MessageDTO create(MessageDTO messageDTO);

    MessageDTO findById(String id);

    MessageDTO update(MessageDTO messageDTO);

    void deleteById(String id);

    List<MessageDTO> findAll();

    List<MessageDTO> findChat(FiltroMessageDTO filtroMessageDTO);

    PagMessageDTO findAllMessagesForAudit(FiltroMessageDTO filtroMessageDTO);
}
