package com.rjd.messenger.service.message;

import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.entity.User;
import com.rjd.messenger.es.service.MessageESService;
import com.rjd.messenger.exceptions.MandatoryFieldException;
import com.rjd.messenger.exceptions.RegistryNotDeletedException;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.repository.MessageRepository;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.util.MessageUtil;
import com.rjd.messenger.util.StringUtils;
import org.apache.avro.generic.GenericData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageServiceImpl extends MessageAbstractService implements MessageService {

    private MessageRepository messageRepository;

    private MessageESService messageESService;

    @Autowired
    public MessageServiceImpl(MyMessageSource myMessageSource, UserService userService, MessageRepository messageRepository, MessageESService messageESService) {
        this.myMessageSource = myMessageSource;
        this.userService = userService;
        this.messageRepository = messageRepository;
        this.messageESService = messageESService;
    }

    @Override
    public MessageDTO create(MessageDTO messageDTO) {
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        if(StringUtils.isNullOrBlank(message.getId())) {
            String id = UUID.randomUUID().toString();
            message.setId(id);
        }
        validateMessage(message);
        Message messageSavedHbase = messageRepository.create(message);
        Message messageSaved = messageESService.create(messageSavedHbase);
        return findById(messageSaved.getId());
    }

    @Override
    public MessageDTO findById(String id) {
        Message message = messageESService.findById(id);
        MessageDTO messageDTO = convertMessageToMessageDTO(message);
        return messageDTO;
    }

    @Override
    public MessageDTO update(MessageDTO messageDTO) {
        //lançará exceção se não houver mensagem no banco
        messageRepository.findById(messageDTO.getId());
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        validateMessageId(message.getId());
        validateMessage(message);
        messageRepository.update(message);
        messageESService.update(message);
        return findById(message.getId());
    }

    @Override
    public void deleteById(String id) {
        messageRepository.findById(id);
        messageRepository.delete(id);
        messageESService.delete(id);
        Message messageNotRemoved = null;
        try{
            messageNotRemoved = messageRepository.findById(id);
        }catch(RegistryNotFoundException e){
            // se caiu aqui então tudo esta certo.
            return;
        }
        if(messageNotRemoved != null) {
            // se chegou aqui tem algo errado
            throw new RegistryNotDeletedException(myMessageSource.get("msg.warn.error.delete.message"));
        }
    }

    @Override
    public List<MessageDTO> findAll() {
        List<Message> messages = messageESService.findAll();
        return convertMessageListToMessageDTOListSort(messages);
    }

    @Override
    public List<MessageDTO> findChat(FiltroMessageDTO filtroMessageDTO) {
        validateReciverIdSenderIdNotNull(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId());
        List<Message> messagesTraded = messageESService.findMessagesFromChat(filtroMessageDTO);
        if(messagesTraded.isEmpty()){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.messages.not.found"));
        }
        return convertMessageListToMessageDTOListSort(messagesTraded);
    }

    @Override
    public PagMessageDTO findAllMessagesForAudit(FiltroMessageDTO filtroMessageDTO) {
        StringUtils.validateIfStringIsInteger(filtroMessageDTO.getNuPage(), "nuPage");
        StringUtils.validateIfStringIsInteger(filtroMessageDTO.getPageSize(), "pageSize");
        return messageESService.findAllMessagesForAudit(filtroMessageDTO);
    }
}
