package com.rjd.messenger.service.user;

import com.rjd.messenger.dto.FiltroUserDTO;
import com.rjd.messenger.dto.PagUserDTO;
import com.rjd.messenger.entity.User;

import java.util.List;

public interface UserService {

    User findById(String id);

    PagUserDTO findAll(String nuPage, String pageSize);

    User create(User user);

    User update(User user);

    void deleteById(String id);

    PagUserDTO findAll(FiltroUserDTO filtro);

    String findUserNameSafeSilenced(String id);
}
