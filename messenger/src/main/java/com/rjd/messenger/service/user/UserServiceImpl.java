package com.rjd.messenger.service.user;


import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroUserDTO;
import com.rjd.messenger.dto.PagUserDTO;
import com.rjd.messenger.entity.User;
import com.rjd.messenger.exceptions.*;
import com.rjd.messenger.repository.UserRepository;
import com.rjd.messenger.util.CollectionUtil;
import com.rjd.messenger.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private Logger logger = Logger.getLogger(this.getClass());

    private MyMessageSource myMessageSource;

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(MyMessageSource myMessageSource, UserRepository userRepository){
        this.myMessageSource = myMessageSource;
        this.userRepository = userRepository;
    }

    @Override
    public User findById(String id) {
        try {
            Optional<User> userOptional = userRepository.findById(id);
            return userOptional.get();
        }catch(NoSuchElementException e){
            throw new RegistryNotFoundException(myMessageSource.get("msg.warn.user.not.found") + " " + id);
        }
    }

    @Override
    public PagUserDTO findAll(String nuPage, String pageSize) throws DataAccessResourceFailureException {
        StringUtils.validateIfStringIsInteger(nuPage, "nuPage");
        StringUtils.validateIfStringIsInteger(pageSize, "pageSize");
        String currentPrincipalName = getAuthenticatedUser();
        Page<User> userPage = userRepository.findByIdNot(currentPrincipalName, PageRequest.of(Integer.valueOf(nuPage), Integer.valueOf(pageSize)));
        return new PagUserDTO(userPage.getNumber() + 1, userPage.getTotalPages(), userPage.getContent());
    }

    @Override
    public PagUserDTO findAll(FiltroUserDTO filtro) {
        Page<User> userPage = findAllUsers(filtro);
        return new PagUserDTO(userPage.getNumber() + 1, userPage.getTotalPages(), userPage.getContent());
    }

    private Page<User> findAllUsers(FiltroUserDTO filtro) {
        StringUtils.validateIfStringIsInteger(filtro.getNuPage(), "nuPage");
        StringUtils.validateIfStringIsInteger(filtro.getPageSize(), "pageSize");
        String currentPrincipalName = getAuthenticatedUser();
        Page<User> userPage = null;
        if(StringUtils.isNotNullOrBlank(filtro.getName()) && StringUtils.isNullOrBlank(filtro.getLastName())) {
            userPage = userRepository.findByIdNotAndNameLike(currentPrincipalName, filtro.getName(), PageRequest.of(Integer.valueOf(filtro.getNuPage()), Integer.valueOf(filtro.getPageSize())));
        }else if (StringUtils.isNullOrBlank(filtro.getName()) && StringUtils.isNotNullOrBlank(filtro.getLastName())){
            userPage = userRepository.findByIdNotAndLastNameLike(currentPrincipalName, filtro.getLastName(), PageRequest.of(Integer.valueOf(filtro.getNuPage()), Integer.valueOf(filtro.getPageSize())));
        }else if (StringUtils.isNotNullOrBlank(filtro.getName()) && StringUtils.isNotNullOrBlank(filtro.getLastName())) {
            userPage = userRepository.findByIdNotAndNameLikeAndLastNameLike(currentPrincipalName, filtro.getName(), filtro.getLastName(), PageRequest.of(Integer.valueOf(filtro.getNuPage()), Integer.valueOf(filtro.getPageSize())));
        }else{
            userPage = userRepository.findByIdNot(currentPrincipalName, PageRequest.of(Integer.valueOf(filtro.getNuPage()), Integer.valueOf(filtro.getPageSize())));
        }
        return userPage;
    }

    @Override
    public User create(User user) {
        validate(user);
        validateUsuarioAlreadyRegistered(user);
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        validate(user);
        User usuarioToUpdate = findById(user.getId());
        usuarioToUpdate.setName(user.getName());
        usuarioToUpdate.setLastName(user.getLastName());
        usuarioToUpdate.setEmail(user.getEmail());
        usuarioToUpdate.setPassword(user.getPassword());
        usuarioToUpdate.setUserRole(user.getUserRole());
        return userRepository.save(usuarioToUpdate);
    }


    @Override
    public void deleteById(String id) {
        findById(id);
        userRepository.deleteById(id);
        User usuarioNotRemoved = null;
        try{
            usuarioNotRemoved = findById(id);
        }catch(RegistryNotFoundException e){
            // se caiu aqui então tudo esta certo.
            return;
        }
        if(usuarioNotRemoved != null) {
            // se chegou aqui tem algo errado
            throw new RegistryNotDeletedException(myMessageSource.get("msg.warn.error.delete.user"));
        }
    }

    @Override
    public String findUserNameSafeSilenced(String id){
        if(StringUtils.isNullOrBlank(id)){
            return null;
        }
        try{
            User user = findById(id);
            return user.getName();
        }catch(RegistryNotFoundException e){
            return null;
        }
    }


    private String getAuthenticatedUser(){
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken) && authentication != null) {
            currentUserName = authentication.getName();
        }
        return currentUserName;
    }

    private void validate(User user) {
        if (StringUtils.isNullOrBlank(user.getId())) {
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.id.mandatory"));
        }
        if (StringUtils.isNullOrBlank(user.getName())) {
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.name.mandatory"));
        }
        if (StringUtils.isNullOrBlank(user.getLastName())) {
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.last.name.mandatory"));
        }
        if (StringUtils.isNullOrBlank(user.getEmail())) {
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.email.mandatory"));
        }
        if (StringUtils.isNullOrBlank(user.getPassword())) {
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.password.mandatory"));
        }
        if (StringUtils.isNullOrBlank(user.getUserRole())) {
            throw new MandatoryFieldException(myMessageSource.get("msg.warn.user.role.mandatory"));
        }
    }


    private void validateUsuarioAlreadyRegistered(User user) {
        try {
            User usuarioFromDB = findById(user.getId());
            if (usuarioFromDB != null) {
                throw new EntityAlreadyRegisteredException(myMessageSource.get("msg.warn.user.already.registered"));
            }
        } catch (RegistryNotFoundException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
