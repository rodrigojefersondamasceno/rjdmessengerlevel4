package com.rjd.messenger.util;

import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.entity.Message;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MessageUtil {
    public static MessageDTO convertMessageToMessageDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        if(message.getId() != null){
            messageDTO.setId(message.getId());
        }
        messageDTO.setSenderId(message.getSenderId());
        messageDTO.setReceiverId(message.getReceiverId());
        messageDTO.setMessage(message.getMessage());
        if(message.getDtMessage() > 0L) {
            messageDTO.setDtMessageLongStr(Long.valueOf(message.getDtMessage()).toString());
        }
        return messageDTO;
    }

    public static Message convertMessageDTOToMessage(MessageDTO messageDTO) {
        Message message = new Message();
        if(StringUtils.isNotNullOrBlank(messageDTO.getId())){
            message.setId(messageDTO.getId());
        }
        message.setSenderId(messageDTO.getSenderId());
        message.setReceiverId(messageDTO.getReceiverId());
        message.setMessage(messageDTO.getMessage());
        if(StringUtils.isNotNullOrBlank(messageDTO.getDtMessageLongStr())){
            message.setDtMessage(Long.parseLong(messageDTO.getDtMessageLongStr()));
        }
        return message;
    }
}
