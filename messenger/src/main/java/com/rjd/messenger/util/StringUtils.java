package com.rjd.messenger.util;

import com.rjd.messenger.exceptions.InvalidFieldException;
import com.rjd.messenger.exceptions.MandatoryFieldException;

public class StringUtils {

    public static boolean isNullOrBlank(String str){
        return str == null || org.springframework.util.StringUtils.isEmpty(str);
    }

    public static boolean isNotNullOrBlank(String str){
        return !isNullOrBlank(str);
    }

    public static boolean isInteger(String str){
        try{
            int i = Integer.parseInt(str);
        }catch(NumberFormatException nfe){
            return false;
        }
        return true;
    }

    public static void validateIfStringIsInteger(String str, String fieldName){
        if(StringUtils.isNullOrBlank(str)){
            throw new MandatoryFieldException("Field '" + fieldName + "' is mandatory." );
        }
        if(!StringUtils.isInteger(str)){
            throw new InvalidFieldException("Field must be Integer: " + fieldName);
        }
    }
}
