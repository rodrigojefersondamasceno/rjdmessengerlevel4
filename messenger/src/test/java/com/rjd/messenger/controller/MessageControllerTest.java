package com.rjd.messenger.controller;

import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.exceptions.MandatoryFieldException;
import com.rjd.messenger.exceptions.RegistryNotDeletedException;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.service.message.MessageService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static com.rjd.messenger.util.JsonUtil.asJsonString;
import static com.rjd.messenger.util.MessageDTOTestUtil.*;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageControllerTest {

    private MockMvc mockMvc;

    private MessageService messageService = mock(MessageService.class);

    private MessageController messageController;

    @Before
    public void setUp(){
        messageController = new MessageController(messageService);
        mockMvc = MockMvcBuilders.standaloneSetup(messageController).build();
    }

    @Test
    public void testRecoverChatSuccess() throws Exception{
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        List<MessageDTO> expectedList = Arrays.asList(getMessageDTO(), getMessageDTO(), getMessageDTO());
        when(messageService.findChat(filtroMessageDTO)).thenReturn(expectedList);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/message/chat").contentType(MediaType.APPLICATION_JSON).content(asJsonString(filtroMessageDTO)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", Matchers.hasSize(3)));
        for(int i = 0; i < expectedList.size(); i++){
            MessageDTO messageDTO = expectedList.get(i);
            resultActions.andExpect(jsonPath("$["+i+"].id", is(messageDTO.getId())))
                    .andExpect(jsonPath("$["+i+"].senderId", is(messageDTO.getSenderId())))
                    .andExpect(jsonPath("$["+i+"].receiverId", is(messageDTO.getReceiverId())))
                    .andExpect(jsonPath("$["+i+"].message", is(messageDTO.getMessage())))
                    .andExpect(jsonPath("$["+i+"].dtMessageLongStr", is(messageDTO.getDtMessageLongStr())));
        }
        verify(messageService, times(1)).findChat(filtroMessageDTO);
    }

    @Test
    public void testRecoverChatFailNotFound() throws Exception{
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        when(messageService.findChat(filtroMessageDTO)).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.post("/message/chat").contentType(MediaType.APPLICATION_JSON).content(asJsonString(filtroMessageDTO)))
                .andExpect(status().isNotFound());
        verify(messageService, times(1)).findChat(filtroMessageDTO);
    }

    @Test
    public void testRecoverChatBadRequest() throws Exception{
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        when(messageService.findChat(filtroMessageDTO)).thenThrow(new MandatoryFieldException());
        mockMvc.perform(MockMvcRequestBuilders.post("/message/chat").contentType(MediaType.APPLICATION_JSON).content(asJsonString(filtroMessageDTO)))
                .andExpect(status().isBadRequest());
        verify(messageService, times(1)).findChat(filtroMessageDTO);
    }

    @Test
    public void testFindMessagesForAudit() throws Exception{
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        List<MessageDTO> expectedList = Arrays.asList(getMessageDTO(), getMessageDTO(), getMessageDTO());
        PagMessageDTO pagMessageDTO = new PagMessageDTO(1, 1, Arrays.asList(getMessageDTO(), getMessageDTO(), getMessageDTO()));
        when(messageService.findAllMessagesForAudit(filtroMessageDTO)).thenReturn(pagMessageDTO);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/message/audit").contentType(MediaType.APPLICATION_JSON).content(asJsonString(filtroMessageDTO)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.messageDTOList", Matchers.hasSize(3)));
        for(int i = 0; i < expectedList.size(); i++){
            MessageDTO messageDTO = expectedList.get(i);
            resultActions.andExpect(jsonPath("$.messageDTOList["+i+"].id", is(messageDTO.getId())))
                    .andExpect(jsonPath("$.messageDTOList["+i+"].senderId", is(messageDTO.getSenderId())))
                    .andExpect(jsonPath("$.messageDTOList["+i+"].receiverId", is(messageDTO.getReceiverId())))
                    .andExpect(jsonPath("$.messageDTOList["+i+"].message", is(messageDTO.getMessage())))
                    .andExpect(jsonPath("$.messageDTOList["+i+"].dtMessageLongStr", is(messageDTO.getDtMessageLongStr())));
        }
        verify(messageService, times(1)).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test
    public void testFindMessagesForAuditFailNotFound() throws Exception{
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        when(messageService.findAllMessagesForAudit(filtroMessageDTO)).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.post("/message/audit").contentType(MediaType.APPLICATION_JSON).content(asJsonString(filtroMessageDTO)))
                .andExpect(status().isNotFound());
        verify(messageService, times(1)).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test
    public void testFindMessagesForAuditBadRequest() throws Exception{
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        when(messageService.findAllMessagesForAudit(filtroMessageDTO)).thenThrow(new MandatoryFieldException());
        mockMvc.perform(MockMvcRequestBuilders.post("/message/audit").contentType(MediaType.APPLICATION_JSON).content(asJsonString(filtroMessageDTO)))
                .andExpect(status().isBadRequest());
        verify(messageService, times(1)).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test
    public void testFindByIdSuccess() throws Exception{
        when(messageService.findById("1")).thenReturn(getMessageDTO());
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/message/{id}", "1"));
        checkResultMessenger(resultActions, status().isOk());
        verify(messageService, times(1)).findById("1");
    }

    @Test
    public void testFindByIdNotFound() throws Exception{
        when(messageService.findById("1")).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.get("/message/{id}", "1")).andExpect(status().isNotFound());
        verify(messageService, times(1)).findById("1");
    }

    @Test
    public void testCreateSuccess() throws Exception {
        MessageDTO message = getMessageDTOWithDateAndIdNull();
        MessageDTO messageCreated = getMessageDTO();
        when(messageService.create(message)).thenReturn(messageCreated);
        checkResultMessenger(mockMvc.perform(MockMvcRequestBuilders.post("/message").contentType(MediaType.APPLICATION_JSON).content(asJsonString(message))),status().isOk());
        verify(messageService, times(1)).create(message);
    }

    @Test
    public void testCreateFailBadRequest() throws Exception {
        MessageDTO message = new MessageDTO();
        when(messageService.create(message)).thenThrow(new MandatoryFieldException());
        mockMvc.perform(MockMvcRequestBuilders.post("/message").contentType(MediaType.APPLICATION_JSON).content(asJsonString(message))).andExpect(status().isBadRequest());
        verify(messageService, times(1)).create(message);
    }

    @Test
    public void testCreateFailNotFound() throws Exception {
        MessageDTO message = getMessageDTOWithDateAndIdNull();
        when(messageService.create(message)).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.post("/message").contentType(MediaType.APPLICATION_JSON).content(asJsonString(message))).andExpect(status().isNotFound());
        verify(messageService, times(1)).create(message);
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        MessageDTO message = getMessageDTO();
        when(messageService.update(message)).thenReturn(message);
        checkResultMessenger(mockMvc.perform(MockMvcRequestBuilders.put("/message").contentType(MediaType.APPLICATION_JSON).content(asJsonString(message))),status().isOk());
        verify(messageService, times(1)).update(message);
    }

    @Test
    public void testUpdateFailBadRequest() throws Exception {
        MessageDTO message = getMessageDTO();
        when(messageService.update(message)).thenThrow(new MandatoryFieldException());
        mockMvc.perform(MockMvcRequestBuilders.put("/message").contentType(MediaType.APPLICATION_JSON).content(asJsonString(message))).andExpect(status().isBadRequest());
        verify(messageService, times(1)).update(message);
    }

    @Test
    public void testUpdateFailNotFound() throws Exception {
        MessageDTO message = getMessageDTO();
        when(messageService.update(message)).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.put("/message").contentType(MediaType.APPLICATION_JSON).content(asJsonString(message))).andExpect(status().isNotFound());
        verify(messageService, times(1)).update(message);
    }

    @Test
    public void testDeleteSuccess() throws Exception {
        doNothing().when(messageService).deleteById("1");
        mockMvc.perform(MockMvcRequestBuilders.delete("/message/{id}", "1")).andExpect(status().isOk());
        verify(messageService, times(1)).deleteById("1");
        doNothing().when(messageService).deleteById("1");
    }

    @Test
    public void testDeleteFailNotFound() throws Exception{
        doThrow(new RegistryNotFoundException()).when(messageService).deleteById("1");
        mockMvc.perform(MockMvcRequestBuilders.delete("/message/{id}", "1"))
                .andExpect(status().isNotFound());
        verify(messageService, times(1)).deleteById("1");
    }

    @Test
    public void testDeleteFailInternalServerError() throws Exception{
        doThrow(new RegistryNotDeletedException()).when(messageService).deleteById("1");
        mockMvc.perform(MockMvcRequestBuilders.delete("/message/{id}", "1"))
                .andExpect(status().isInternalServerError());
        verify(messageService, times(1)).deleteById("1");
    }

    private void checkResultMessenger(ResultActions resultActions, ResultMatcher ok) throws Exception {
        MessageDTO messageExpected = getMessageDTO();
        resultActions.andExpect(ok)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.senderId", is(messageExpected.getSenderId())))
                .andExpect(jsonPath("$.receiverId", is(messageExpected.getReceiverId())))
                .andExpect(jsonPath("$.message", is(messageExpected.getMessage())));
    }
}
