package com.rjd.messenger.controller;

import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.util.MessageDTOTestUtil;
import com.rjd.messenger.util.UserTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MessageWebSocketControllerTest {
    private String url;
    @Value("${local.server.port}")
    private int port;

    private WebSocketStompClient stompClient;

    private static final String SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT = "/queue/listen";

    @Before
    public void setUp(){
        url = "ws://localhost:"+ port +"/messenger";
        stompClient = new WebSocketStompClient(new SockJsClient(Arrays.asList(new WebSocketTransport(new StandardWebSocketClient()))));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
    }

    @Test
    public void testQueueListen1UserSuccess() throws InterruptedException, ExecutionException, TimeoutException {
        StompSession session = stompClient.connect(url, new StompSessionHandlerAdapter() {}).get(1, TimeUnit.SECONDS);
        MessageDTOStompFrameHandler stompFrameHandler = new MessageDTOStompFrameHandler();

        session.subscribe(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT + "/user1", stompFrameHandler);
        MessageDTO messageDTOToSent = MessageDTOTestUtil.getMessageDTO();
        session.send(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT  + "/user1", messageDTOToSent);

        CompletableFuture<MessageDTO> completableFutureUser1 = stompFrameHandler.getInnerCompletableFuture();

        MessageDTO messageDTOSent = completableFutureUser1.get(5,TimeUnit.SECONDS);

        checkMessageDTO(messageDTOToSent, messageDTOSent);
    }

    @Test
    public void testQueueListen2UsersSuccess() throws InterruptedException, ExecutionException, TimeoutException {
        StompSession sessionUser1 = stompClient.connect(url, new StompSessionHandlerAdapter() {}).get(1, TimeUnit.SECONDS);
        StompSession sessionUser2 = stompClient.connect(url, new StompSessionHandlerAdapter() {}).get(1, TimeUnit.SECONDS);
        MessageDTOStompFrameHandler stompFrameHandlerUser1 = new MessageDTOStompFrameHandler();
        MessageDTOStompFrameHandler stompFrameHandlerUser2 = new MessageDTOStompFrameHandler();

        MessageDTO messageDTOToUser1 = MessageDTOTestUtil.getMessageDTO();
        messageDTOToUser1.setReceiverId(UserTestUtil.ID_USER_COMUM);
        messageDTOToUser1.setReceiverName(UserTestUtil.ID_USER_COMUM);
        messageDTOToUser1.setMessage("mensagem teste User1");
        MessageDTO messageDTOToUser2 = MessageDTOTestUtil.getMessageDTO();
        messageDTOToUser2.setMessage("mensagem teste User2");

        sessionUser1.subscribe(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT + "/user1", stompFrameHandlerUser1);
        sessionUser1.send(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT  + "/user1", messageDTOToUser1);

        sessionUser2.subscribe(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT + "/user2", stompFrameHandlerUser2);
        sessionUser2.send(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT  + "/user2", messageDTOToUser2);

        CompletableFuture<MessageDTO> completableFutureUser1 = stompFrameHandlerUser1.getInnerCompletableFuture();
        CompletableFuture<MessageDTO> completableFutureUser2 = stompFrameHandlerUser2.getInnerCompletableFuture();

        MessageDTO messageDTOSentUser1 = completableFutureUser1.get(5,TimeUnit.SECONDS);
        MessageDTO messageDTOSentUser2 = completableFutureUser2.get(5,TimeUnit.SECONDS);

        checkMessageDTO(messageDTOToUser1, messageDTOSentUser1);
        checkMessageDTO(messageDTOToUser2, messageDTOSentUser2);
    }

    @Test(expected = TimeoutException.class)
    public void testQueueListenFail() throws InterruptedException, ExecutionException, TimeoutException {
        StompSession session = stompClient.connect(url, new StompSessionHandlerAdapter() {}).get(1, TimeUnit.SECONDS);
        MessageDTOStompFrameHandler stompFrameHandler = new MessageDTOStompFrameHandler();

        session.subscribe(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT + "/user1", stompFrameHandler);
        MessageDTO messageDTOToSent = MessageDTOTestUtil.getMessageDTO();
        session.send(SUBSCRIBE_SEND_QUEUE_LISTEN_ENDPOINT  + "/user2", messageDTOToSent);

        CompletableFuture<MessageDTO> completableFutureUser1 = stompFrameHandler.getInnerCompletableFuture();

        completableFutureUser1.get(5,TimeUnit.SECONDS);
    }


    private void checkMessageDTO(MessageDTO messageDTOExpected, MessageDTO messageDTOResult) {
        assertNotNull("MenssageDTO not null", messageDTOResult);
        assertEquals("SenderId as expected", messageDTOExpected.getSenderId(), messageDTOResult.getSenderId());
        assertEquals("SenderName as expected",messageDTOExpected.getSenderName(), messageDTOResult.getSenderName());
        assertEquals("ReceiverId as expected", messageDTOExpected.getReceiverId(), messageDTOResult.getReceiverId());
        assertEquals("ReceiverName as expected",messageDTOExpected.getReceiverName(), messageDTOResult.getReceiverName());
        assertEquals("Message as expected",messageDTOExpected.getMessage(), messageDTOResult.getMessage());
    }

    private class MessageDTOStompFrameHandler implements StompFrameHandler{
        private CompletableFuture<MessageDTO> innerCompletableFuture;

        public MessageDTOStompFrameHandler() {
            innerCompletableFuture = new CompletableFuture<>();
        }

        public CompletableFuture<MessageDTO> getInnerCompletableFuture() {
            return innerCompletableFuture;
        }

        public void setInnerCompletableFuture(CompletableFuture<MessageDTO> innerCompletableFuture) {
            this.innerCompletableFuture = innerCompletableFuture;
        }

        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return MessageDTO.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            innerCompletableFuture.complete((MessageDTO) o);
        }
    }
}
