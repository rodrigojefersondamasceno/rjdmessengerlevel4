package com.rjd.messenger.controller;

import com.rjd.messenger.dto.FiltroUserDTO;
import com.rjd.messenger.dto.PagUserDTO;
import com.rjd.messenger.exceptions.*;
import com.rjd.messenger.util.UserTestUtil;
import com.rjd.messenger.entity.User;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.util.JsonUtil;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    private MockMvc mockMvc;

    private UserService userService = mock(UserService.class);

    private UserController userController;

    @Before
    public void setUp(){
        userController = new UserController(userService);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void testFindAllSuccess() throws Exception{
        List<User> userList = UserTestUtil.getUserList();
        PagUserDTO pagUserDTO = new PagUserDTO(0, 10, userList);
        when(userService.findAll("0", "10")).thenReturn(pagUserDTO);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/user").param("nuPage", "0").param("pageSize", "10"));
                resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.usersList", Matchers.hasSize(2)));
                for(User user : userList){
                    int index = userList.indexOf(user);
                    resultActions.andExpect(jsonPath("$.usersList["+index+"].id", is(user.getId())))
                            .andExpect(jsonPath("$.usersList["+index+"].name", is(user.getName())))
                            .andExpect(jsonPath("$.usersList["+index+"].lastName", is(user.getLastName())))
                            .andExpect(jsonPath("$.usersList["+index+"].email", is(user.getEmail())))
                            .andExpect(jsonPath("$.usersList["+index+"].password", is(user.getPassword())))
                            .andExpect(jsonPath("$.usersList["+index+"].userRole", is(user.getUserRole())));
                }
        verify(userService, times(1)).findAll("0", "10");
    }

    @Test
    public void testFindAllEmptySuccess() throws Exception{
        when(userService.findAll("0", "10")).thenReturn(new PagUserDTO());
        mockMvc.perform(MockMvcRequestBuilders.get("/user").param("nuPage", "0").param("pageSize", "10"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.usersList", Matchers.hasSize(0)));
        verify(userService, times(1)).findAll("0", "10");
    }

    @Test
    public void testFindAllFail400BadRequest() throws Exception{
        when(userService.findAll("1","a")).thenThrow(new InvalidFieldException());
        mockMvc.perform(MockMvcRequestBuilders.get("/user").param("nuPage", "1").param("pageSize", "a"))
                .andExpect(status().isBadRequest());
        verify(userService, times(1)).findAll("1","a");
    }

    @Test
    public void testFindAllWithFilterSuccess() throws Exception{
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),"0","10");
        List<User> userList = UserTestUtil.getUserList();
        PagUserDTO pagUserDTO = new PagUserDTO(0, 10, userList);
        when(userService.findAll(filtroUserDTO)).thenReturn(pagUserDTO);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/user/filter").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(filtroUserDTO)));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.usersList", Matchers.hasSize(2)));
        for(User user : userList){
            int index = userList.indexOf(user);
            resultActions.andExpect(jsonPath("$.usersList["+index+"].id", is(user.getId())))
                    .andExpect(jsonPath("$.usersList["+index+"].name", is(user.getName())))
                    .andExpect(jsonPath("$.usersList["+index+"].lastName", is(user.getLastName())))
                    .andExpect(jsonPath("$.usersList["+index+"].email", is(user.getEmail())))
                    .andExpect(jsonPath("$.usersList["+index+"].password", is(user.getPassword())))
                    .andExpect(jsonPath("$.usersList["+index+"].userRole", is(user.getUserRole())));
        }
        verify(userService, times(1)).findAll(filtroUserDTO);
    }

    @Test
    public void testFindAllWithFilterEmptySuccess() throws Exception{
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),"0","10");
        when(userService.findAll(filtroUserDTO)).thenReturn(new PagUserDTO());
        mockMvc.perform(MockMvcRequestBuilders.post("/user/filter").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(filtroUserDTO)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.usersList", Matchers.hasSize(0)));
        verify(userService, times(1)).findAll(filtroUserDTO);
    }

    @Test
    public void testFindAllWithFilterFail400BadRequest() throws Exception{
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),null,"10");
        when(userService.findAll(filtroUserDTO)).thenThrow(new InvalidFieldException());
        mockMvc.perform(MockMvcRequestBuilders.post("/user/filter").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(filtroUserDTO)))
                .andExpect(status().isBadRequest());
        verify(userService, times(1)).findAll(filtroUserDTO);
    }

    @Test
    public void testFindByIdSuccess() throws Exception{
        when(userService.findById(UserTestUtil.ID_USER_ADMIN)).thenReturn(UserTestUtil.getUserAdmin());
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", UserTestUtil.ID_USER_ADMIN));
        checkResultUserTestAdmin(resultActions, status().isOk());
        verify(userService, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test
    public void testFindById404NotFound() throws Exception{
        when(userService.findById(UserTestUtil.ID_USER_ADMIN)).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", UserTestUtil.ID_USER_ADMIN))
                .andExpect(status().isNotFound());
        verify(userService, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test
    public void testCreateSuccess() throws Exception{
        when(userService.create(UserTestUtil.getUserAdmin())).thenReturn(UserTestUtil.getUserAdmin());
        checkResultUserTestAdmin(mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(UserTestUtil.getUserAdmin()))), status().isOk());
        verify(userService, times(1)).create(UserTestUtil.getUserAdmin());
    }

    @Test
    public void testCreateFail409Conflict() throws Exception{
        when(userService.create(UserTestUtil.getUserAdmin())).thenThrow(new EntityAlreadyRegisteredException());
        mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(UserTestUtil.getUserAdmin())))
                .andExpect(status().isConflict());
        verify(userService, times(1)).create(UserTestUtil.getUserAdmin());
    }

    @Test
    public void testCreateFail400BadRequest() throws Exception{
        User usuario = new User();
        when(userService.create(usuario)).thenThrow(new MandatoryFieldException());
        mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(usuario)))
                .andExpect(status().isBadRequest());
        verify(userService, times(1)).create(usuario);
    }

    @Test
    public void testUpdateSuccess() throws Exception{
        when(userService.update(UserTestUtil.getUserAdmin())).thenReturn(UserTestUtil.getUserAdmin());
        checkResultUserTestAdmin(mockMvc.perform(MockMvcRequestBuilders.put("/user").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(UserTestUtil.getUserAdmin()))), status().isOk());
        verify(userService, times(1)).update(UserTestUtil.getUserAdmin());
    }

    @Test
    public void testUpdateFail400BadRequest() throws Exception{
        User usuario = new User();
        when(userService.update(usuario)).thenThrow(new MandatoryFieldException());
        mockMvc.perform(MockMvcRequestBuilders.put("/user").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(usuario)))
                .andExpect(status().isBadRequest());
        verify(userService, times(1)).update(usuario);
    }

    @Test
    public void testUpdateFail404NotFound() throws Exception{
        when(userService.update(UserTestUtil.getUserAdmin())).thenThrow(new RegistryNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.put("/user").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.asJsonString(UserTestUtil.getUserAdmin())))
                .andExpect(status().isNotFound());
        verify(userService, times(1)).update(UserTestUtil.getUserAdmin());
    }

    @Test
    public void testDeleteSuccess() throws Exception{
        doNothing().when(userService).deleteById(UserTestUtil.ID_USER_ADMIN);
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", UserTestUtil.ID_USER_ADMIN))
                .andExpect(status().isOk());
        verify(userService, times(1)).deleteById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test
    public void testDeleteFail404NotFound() throws Exception{
        doThrow(new RegistryNotFoundException()).when(userService).deleteById(UserTestUtil.ID_USER_ADMIN);
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", UserTestUtil.ID_USER_ADMIN))
                .andExpect(status().isNotFound());
        verify(userService, times(1)).deleteById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test
    public void testDeleteUsuarioFail500InternalServerError() throws Exception{
        doThrow(new RegistryNotDeletedException()).when(userService).deleteById(UserTestUtil.ID_USER_ADMIN);
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", UserTestUtil.ID_USER_ADMIN))
                .andExpect(status().isInternalServerError());
        verify(userService, times(1)).deleteById(UserTestUtil.ID_USER_ADMIN);
    }

    private void checkResultUserTestAdmin(ResultActions resultActions, ResultMatcher ok) throws Exception {
        User userAdmin = UserTestUtil.getUserAdmin();
        resultActions.andExpect(ok)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(userAdmin.getId())))
                .andExpect(jsonPath("$.name", is(userAdmin.getName())))
                .andExpect(jsonPath("$.lastName", is(userAdmin.getLastName())))
                .andExpect(jsonPath("$.email", is(userAdmin.getEmail())))
                .andExpect(jsonPath("$.password", is(userAdmin.getPassword())))
                .andExpect(jsonPath("$.userRole", is(userAdmin.getUserRole())));
    }

}
