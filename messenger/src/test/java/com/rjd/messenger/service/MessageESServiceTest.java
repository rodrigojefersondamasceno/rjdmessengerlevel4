package com.rjd.messenger.service;

import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.es.repository.MessageESRepository;
import com.rjd.messenger.es.service.MessageESService;
import com.rjd.messenger.es.service.MessageESServiceImpl;
import com.rjd.messenger.exceptions.InvalidFieldException;
import com.rjd.messenger.exceptions.MandatoryFieldException;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.util.MessageUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.rjd.messenger.util.MessageDTOTestUtil.getFiltroMessageDTO;
import static com.rjd.messenger.util.MessageDTOTestUtil.getMessageDTO;
import static com.rjd.messenger.util.UserTestUtil.getUserAdmin;
import static com.rjd.messenger.util.UserTestUtil.getUserComum;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageESServiceTest {

    private MessageESService messageESService;

    private MessageESRepository messageESRepository = mock(MessageESRepository.class);

    private UserService userService = mock(UserService.class);

    private MyMessageSource myMessageSource = mock(MyMessageSource.class);

    @Before
    public void setUp(){
        messageESService = new MessageESServiceImpl(messageESRepository, userService, myMessageSource);
    }

    @Test
    public void findMessagesFromChatSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPageSent = new PageImpl<>(Arrays.asList(message));
        Page<Message> messagesPageReceived = new PageImpl<>(Arrays.asList(message));
        List<Message> expectedResult = Arrays.asList(message, message);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId())).thenReturn(messagesPageSent);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId())).thenReturn(messagesPageReceived);
        List<Message> messageListReturned = messageESService.findMessagesFromChat(filtroMessageDTO);
        checkMessageListResult(messageListReturned, expectedResult);
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId());
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId());
    }

    @Test
    public void findMessagesFromChatSuccessNoMessageSent(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPageSent = new PageImpl<>(new ArrayList<>());
        Page<Message> messagesPageReceived = new PageImpl<>(Arrays.asList(message));
        List<Message> expectedResult = Arrays.asList(message);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId())).thenReturn(messagesPageSent);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId())).thenReturn(messagesPageReceived);
        List<Message> messageListReturned = messageESService.findMessagesFromChat(filtroMessageDTO);
        checkMessageListResult(messageListReturned, expectedResult);
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId());
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId());
    }

    @Test
    public void findMessagesFromChatSuccessNoMessageReceived(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPageSent = new PageImpl<>(Arrays.asList(message));
        Page<Message> messagesPageReceived = new PageImpl<>(new ArrayList<>());
        List<Message> expectedResult = Arrays.asList(message);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId())).thenReturn(messagesPageSent);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId())).thenReturn(messagesPageReceived);
        List<Message> messageListReturned = messageESService.findMessagesFromChat(filtroMessageDTO);
        checkMessageListResult(messageListReturned, expectedResult);
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId());
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId());
    }

    @Test(expected = RegistryNotFoundException.class)
    public void findMessagesFromChatFailNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        Page<Message> messagesPageEmpty = new PageImpl<>(new ArrayList<>());
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId())).thenReturn(messagesPageEmpty);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId())).thenReturn(messagesPageEmpty);
        messageESService.findMessagesFromChat(filtroMessageDTO);
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId());
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getReceiverId(), filtroMessageDTO.getSenderId());
    }

    @Test
    public void findAllMessagesForAuditSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        Pageable page = PageRequest.of(0, 10);
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPage = new PageImpl<>(Arrays.asList(message, message));
        List<Message> expectedResult = Arrays.asList(message, message);
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId(), page)).thenReturn(messagesPage);
        PagMessageDTO pagMessageDTO = messageESService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), expectedResult);
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId(), page);
    }

    @Test
    public void findAllMessagesForAuditNoSenderIdSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setSenderId(null);
        Pageable page = PageRequest.of(0, 10);
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPage = new PageImpl<>(Arrays.asList(message, message));
        List<Message> expectedResult = Arrays.asList(message, message);
        when(messageESRepository.findByReceiverId(filtroMessageDTO.getReceiverId(), page)).thenReturn(messagesPage);
        PagMessageDTO pagMessageDTO = messageESService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), expectedResult);
        verify(messageESRepository, times(1 )).findByReceiverId(filtroMessageDTO.getReceiverId(), page);
    }

    @Test
    public void findAllMessagesForAuditNoReceiverIdSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setReceiverId(null);
        Pageable page = PageRequest.of(0, 10);
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPage = new PageImpl<>(Arrays.asList(message, message));
        List<Message> expectedResult = Arrays.asList(message, message);
        when(messageESRepository.findBySenderId(filtroMessageDTO.getSenderId(), page)).thenReturn(messagesPage);
        PagMessageDTO pagMessageDTO = messageESService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), expectedResult);
        verify(messageESRepository, times(1 )).findBySenderId(filtroMessageDTO.getSenderId(), page);
    }

    @Test
    public void findAllMessagesForAuditNoSenderIdANDReceiverIdSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setReceiverId(null);
        filtroMessageDTO.setSenderId(null);
        Pageable page = PageRequest.of(0, 10);
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        Page<Message> messagesPage = new PageImpl<>(Arrays.asList(message, message));
        List<Message> expectedResult = Arrays.asList(message, message);
        when(messageESRepository.findAll(page)).thenReturn(messagesPage);
        PagMessageDTO pagMessageDTO = messageESService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), expectedResult);
        verify(messageESRepository, times(1 )).findAll(page);
    }

    @Test(expected = InvalidFieldException.class)
    public void findAllMessagesForAuditFailNuPageInvalid(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setNuPage("a");
        messageESService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = InvalidFieldException.class)
    public void findAllMessagesForAuditFailPageSizeInvalid(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setPageSize("a");
        messageESService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void findAllMessagesForAuditFailNuPageNull(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setNuPage(null);
        messageESService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void findAllMessagesForAuditFailPageSizeNull(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setPageSize(null);
        messageESService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void findAllMessagesForAuditFailNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        Pageable page = PageRequest.of(0, 10);
        Page<Message> messagesPage = new PageImpl<>(new ArrayList<>());
        when(messageESRepository.findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId(), page)).thenReturn(messagesPage);
        messageESService.findAllMessagesForAudit(filtroMessageDTO);
        verify(messageESRepository, times(1 )).findBySenderIdAndReceiverId(filtroMessageDTO.getSenderId(), filtroMessageDTO.getReceiverId(), page);
    }

    private void checkMessageListResult(List<Message> messageListReturned, List<Message> expectedResult) {
        assertNotNull("Result is null", messageListReturned);
        assertEquals("Result has size different from expected", expectedResult.size(), messageListReturned.size());
        for(int i = 0; i < messageListReturned.size(); i++){
            assertEquals("Name different from expected value.", expectedResult.get(i).getSenderId(), messageListReturned.get(i).getSenderId());
            assertEquals("LastName different from expected value.", expectedResult.get(i).getReceiverId(), messageListReturned.get(i).getReceiverId());
            assertEquals("Email different from expected value.", expectedResult.get(i).getMessage(), messageListReturned.get(i).getMessage());
        }
    }

    private void checkMessageDTOListResult(List<MessageDTO> messageDTOListReturned, List<Message> expectedResult) {
        assertNotNull("Result is null", messageDTOListReturned);
        assertEquals("Result has size different from expected", expectedResult.size(), messageDTOListReturned.size());
        for(int i = 0; i < messageDTOListReturned.size(); i++){
            assertEquals("Name different from expected value.", expectedResult.get(i).getSenderId(), messageDTOListReturned.get(i).getSenderId());
            assertEquals("LastName different from expected value.", expectedResult.get(i).getReceiverId(), messageDTOListReturned.get(i).getReceiverId());
            assertEquals("Email different from expected value.", expectedResult.get(i).getMessage(), messageDTOListReturned.get(i).getMessage());
        }
    }
}
