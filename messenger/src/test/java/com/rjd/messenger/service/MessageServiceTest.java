package com.rjd.messenger.service;


import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.dto.PagMessageDTO;
import com.rjd.messenger.entity.Message;
import com.rjd.messenger.es.service.MessageESService;
import com.rjd.messenger.exceptions.InvalidFieldException;
import com.rjd.messenger.exceptions.MandatoryFieldException;
import com.rjd.messenger.exceptions.RegistryNotDeletedException;
import com.rjd.messenger.exceptions.RegistryNotFoundException;
import com.rjd.messenger.repository.MessageRepository;
import com.rjd.messenger.service.message.MessageService;
import com.rjd.messenger.service.message.MessageServiceImpl;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.util.MessageUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static com.rjd.messenger.util.MessageDTOTestUtil.getFiltroMessageDTO;
import static com.rjd.messenger.util.MessageDTOTestUtil.getMessageDTO;
import static com.rjd.messenger.util.UserTestUtil.getUserAdmin;
import static com.rjd.messenger.util.UserTestUtil.getUserComum;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageServiceTest {

    private MyMessageSource myMessageSource = mock(MyMessageSource.class);

    private UserService userService = mock(UserService.class);

    private MessageRepository messageRepository = mock(MessageRepository.class);

    private MessageService messageService;

    private MessageESService messageESService = mock(MessageESService.class);

    @Before
    public void setUp(){
        messageService = new MessageServiceImpl(myMessageSource, userService, messageRepository, messageESService);
    }

    @Test
    public void findChatSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        List<Message> expectedResult = Arrays.asList(MessageUtil.convertMessageDTOToMessage(messageDTO), MessageUtil.convertMessageDTOToMessage(messageDTO));
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageESService.findMessagesFromChat(filtroMessageDTO)).thenReturn(expectedResult);
        List<MessageDTO> messageDTOListReturned = messageService.findChat(filtroMessageDTO);
        checkMessageDTOListResult(messageDTOListReturned, expectedResult);
        verify(userService, times(2 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(2 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findMessagesFromChat(filtroMessageDTO);
    }

    @Test
    public void findChatSuccessSenderNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        List<Message> exectedResult = Arrays.asList(MessageUtil.convertMessageDTOToMessage(messageDTO), MessageUtil.convertMessageDTOToMessage(messageDTO));
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(null);
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageESService.findMessagesFromChat(filtroMessageDTO)).thenReturn(exectedResult);
        List<MessageDTO> messageDTOListReturned = messageService.findChat(filtroMessageDTO);
        checkMessageDTOListResult(messageDTOListReturned, exectedResult);
        verify(userService, times(2 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(2 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findMessagesFromChat(filtroMessageDTO);
    }

    @Test
    public void findChatSuccessReceiverNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        List<Message> expectedResult = Arrays.asList(MessageUtil.convertMessageDTOToMessage(messageDTO), MessageUtil.convertMessageDTOToMessage(messageDTO));
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(null);
        when(messageESService.findMessagesFromChat(filtroMessageDTO)).thenReturn(expectedResult);
        List<MessageDTO> messageDTOListReturned = messageService.findChat(filtroMessageDTO);
        checkMessageDTOListResult(messageDTOListReturned, expectedResult);
        verify(userService, times(2 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(2 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findMessagesFromChat(filtroMessageDTO);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void findChatFailNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        when(messageRepository.findMessagesFromChat(filtroMessageDTO)).thenThrow(new RegistryNotFoundException());
        messageService.findChat(filtroMessageDTO);
        verify(messageRepository, times(1 )).findMessagesFromChat(filtroMessageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void findChatFailSenderNull(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setSenderId(null);
        messageService.findChat(filtroMessageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void findChatFailReceiverNull(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setReceiverId(null);
        messageService.findChat(filtroMessageDTO);
    }

    @Test
    public void findAllMessagesForAuditSuccess(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        List<Message> expectedResult = Arrays.asList(MessageUtil.convertMessageDTOToMessage(messageDTO), MessageUtil.convertMessageDTOToMessage(messageDTO));
        PagMessageDTO pagMessageDTOExpected = new PagMessageDTO(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize()), Arrays.asList(messageDTO, messageDTO));
        when(messageESService.findAllMessagesForAudit(filtroMessageDTO)).thenReturn(pagMessageDTOExpected);
        PagMessageDTO pagMessageDTO = messageService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), expectedResult);
        verify(messageESService, times(1 )).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test
    public void findAllMessagesForAuditSuccessSenderNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        List<Message> exectedResult = Arrays.asList(MessageUtil.convertMessageDTOToMessage(messageDTO), MessageUtil.convertMessageDTOToMessage(messageDTO));
        PagMessageDTO pagMessageDTOExpected = new PagMessageDTO(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize()), Arrays.asList(messageDTO, messageDTO));
        when(messageESService.findAllMessagesForAudit(filtroMessageDTO)).thenReturn(pagMessageDTOExpected);
        PagMessageDTO pagMessageDTO = messageService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), exectedResult);
        verify(messageESService, times(1 )).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test
    public void findAllMessagesForAuditSuccessReceiverNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        MessageDTO messageDTO = getMessageDTO();
        List<Message> expectedResult = Arrays.asList(MessageUtil.convertMessageDTOToMessage(messageDTO), MessageUtil.convertMessageDTOToMessage(messageDTO));
        PagMessageDTO pagMessageDTOExpected = new PagMessageDTO(Integer.valueOf(filtroMessageDTO.getNuPage()), Integer.valueOf(filtroMessageDTO.getPageSize()), Arrays.asList(messageDTO, messageDTO));
        when(messageESService.findAllMessagesForAudit(filtroMessageDTO)).thenReturn(pagMessageDTOExpected);
        PagMessageDTO pagMessageDTO = messageService.findAllMessagesForAudit(filtroMessageDTO);
        checkMessageDTOListResult(pagMessageDTO.getMessageDTOList(), expectedResult);
        verify(messageESService, times(1 )).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void findAllMessagesForAuditFailNotFound(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        when(messageRepository.findAllMessagesForAudit(filtroMessageDTO)).thenThrow(new RegistryNotFoundException());
        messageService.findChat(filtroMessageDTO);
        verify(messageRepository, times(1 )).findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void findAllMessagesForAuditFailNuPageNull(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setNuPage(null);
        messageService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = InvalidFieldException.class)
    public void findAllMessagesForAuditFailNuPageNotInteger(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setNuPage("a");
        messageService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void findAllMessagesForAuditFailPageSizeNull(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setPageSize(null);
        messageService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test(expected = InvalidFieldException.class)
    public void findAllMessagesForAuditFailPageSizeNotInteger(){
        FiltroMessageDTO filtroMessageDTO = getFiltroMessageDTO();
        filtroMessageDTO.setPageSize("a");
        messageService.findAllMessagesForAudit(filtroMessageDTO);
    }

    @Test
    public void findByIdSuccess(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageReturned = messageService.findById(message.getId());
        checkMessageResult(messageReturned, getMessageDTO());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findById(message.getId());
    }

    @Test
    public void findByIdSuccessSenderIdNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setSenderId(null);
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(null);
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageReturned = messageService.findById(message.getId());
        checkMessageResult(messageReturned, messageDTO);
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findById(message.getId());
    }

    @Test
    public void findByIdSuccessReceiverIdNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setReceiverId(null);
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(null);
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageReturned = messageService.findById(message.getId());
        checkMessageResult(messageReturned, messageDTO);
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findById(message.getId());
    }

    @Test
    public void findByIdSuccessSenderNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(null);
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageReturned = messageService.findById(message.getId());
        checkMessageResult(messageReturned, getMessageDTO());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findById(message.getId());
    }

    @Test
    public void findByIdSuccessRecieverNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(null);
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageReturned = messageService.findById(message.getId());
        checkMessageResult(messageReturned, getMessageDTO());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findById(message.getId());
    }

    @Test(expected = RegistryNotFoundException.class)
    public void findByIdFail(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageESService.findById(messageDTO.getId())).thenThrow(new RegistryNotFoundException());
        messageService.findById(message.getId());
        verify(messageESService, times(1 )).findById(message.getId());
    }

    @Test
    public void testCreateSuccess(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findById(messageDTO.getSenderId())).thenReturn(getUserAdmin());
        when(userService.findById(messageDTO.getReceiverId())).thenReturn(getUserComum());
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        when(messageRepository.create(message)).thenReturn(message);
        when(messageESService.create(message)).thenReturn(message);
        MessageDTO messageCreated = messageService.create(messageDTO);
        checkMessageResult(messageCreated, getMessageDTO());
        verify(userService, times(1 )).findById(messageDTO.getSenderId());
        verify(userService, times(1 )).findById(messageDTO.getReceiverId());
        verify(messageESService, times(1 )).findById(message.getId());
        verify(messageRepository, times(1 )).create(message);
        verify(messageESService, times(1)).create(message);
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailSenderNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setSenderId(null);
        messageService.create(messageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailReceiverNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setReceiverId(null);
        messageService.create(messageDTO);
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailMessageNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setMessage(null);
        messageService.create(messageDTO);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testCreateFailSenderNotfound(){
        MessageDTO messageDTO = getMessageDTO();
        when(userService.findById(messageDTO.getSenderId())).thenThrow(new RegistryNotFoundException());
        MessageDTO messageCreated = messageService.create(messageDTO);
        checkMessageResult(messageCreated, getMessageDTO());
        verify(userService, times(1 )).findById(messageDTO.getSenderId());
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testCreateFailReceiverNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        when(userService.findById(messageDTO.getSenderId())).thenReturn(getUserAdmin());
        when(userService.findById(messageDTO.getReceiverId())).thenThrow(new RegistryNotFoundException());
        MessageDTO messageCreated = messageService.create(messageDTO);
        checkMessageResult(messageCreated, getMessageDTO());
        verify(userService, times(1 )).findById(messageDTO.getSenderId());
        verify(userService, times(1 )).findById(messageDTO.getReceiverId());
    }

    @Test
    public void testUpdateSuccess(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findById(messageDTO.getSenderId())).thenReturn(getUserAdmin());
        when(userService.findById(messageDTO.getReceiverId())).thenReturn(getUserComum());
        when(userService.findUserNameSafeSilenced(messageDTO.getSenderId())).thenReturn(getUserAdmin().getName());
        when(userService.findUserNameSafeSilenced(messageDTO.getReceiverId())).thenReturn(getUserComum().getName());
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        when(messageESService.findById(messageDTO.getId())).thenReturn(message);
        when(messageRepository.update(message)).thenReturn(message);
        MessageDTO messageCreated = messageService.update(messageDTO);
        checkMessageResult(messageCreated, getMessageDTO());
        verify(userService, times(1 )).findById(messageDTO.getSenderId());
        verify(userService, times(1 )).findById(messageDTO.getReceiverId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getSenderId());
        verify(userService, times(1 )).findUserNameSafeSilenced(messageDTO.getReceiverId());
        verify(messageRepository, times(1 )).findById(message.getId());
        verify(messageESService, times(1 )).findById(message.getId());
        verify(messageRepository, times(1 )).update(message);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testUpdateFailMessageNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageRepository.findById(messageDTO.getId())).thenThrow(new RegistryNotFoundException());
        messageService.update(messageDTO);
        verify(messageRepository, times(2 )).findById(message.getId());
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailSenderNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setSenderId(null);
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        messageService.update(messageDTO);
        verify(messageRepository, times(1 )).findById(message.getId());
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailReceiverNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setReceiverId(null);
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        messageService.update(messageDTO);
        verify(messageRepository, times(1 )).findById(message.getId());
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailMessageNull(){
        MessageDTO messageDTO = getMessageDTO();
        messageDTO.setMessage(null);
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        messageService.update(messageDTO);
        verify(messageRepository, times(1 )).findById(message.getId());
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testUpdateFailSenderNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findById(messageDTO.getSenderId())).thenThrow(new RegistryNotFoundException());
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageCreated = messageService.update(messageDTO);
        verify(userService, times(1 )).findById(messageDTO.getSenderId());
        verify(messageRepository, times(1 )).findById(message.getId());
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testUpdateFailReceiverNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(userService.findById(messageDTO.getSenderId())).thenReturn(getUserAdmin());
        when(userService.findById(messageDTO.getReceiverId())).thenThrow(new RegistryNotFoundException());
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        MessageDTO messageCreated = messageService.update(messageDTO);
        checkMessageResult(messageCreated, getMessageDTO());
        verify(userService, times(1 )).findById(messageDTO.getSenderId());
        verify(userService, times(1 )).findById(messageDTO.getReceiverId());
        verify(messageRepository, times(1 )).findById(message.getId());
    }

    @Test
    public void testDeleteSuccess(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message).thenThrow(new RegistryNotFoundException());;
        messageService.deleteById(messageDTO.getId());
        verify(messageRepository, times(2 )).findById(messageDTO.getId());
    }


    @Test(expected = RegistryNotFoundException.class)
    public void testDeleteFailNotFound(){
        MessageDTO messageDTO = getMessageDTO();
        when(messageRepository.findById(messageDTO.getId())).thenThrow(new RegistryNotFoundException());;
        messageService.deleteById(messageDTO.getId());
        verify(messageRepository, times(1 )).findById(messageDTO.getId());
    }

    @Test(expected = RegistryNotDeletedException.class)
    public void testDeleteFailDataNotDeleted(){
        MessageDTO messageDTO = getMessageDTO();
        Message message = MessageUtil.convertMessageDTOToMessage(messageDTO);
        when(messageRepository.findById(messageDTO.getId())).thenReturn(message);
        messageService.deleteById(messageDTO.getId());
        verify(messageRepository, times(2 )).findById(messageDTO.getId());
    }



    private void checkMessageDTOListResult(List<MessageDTO> messageDTOListReturned, List<Message> expectedResult) {
        assertNotNull("Result is null", messageDTOListReturned);
        assertEquals("Result has size different from expected", expectedResult.size(), messageDTOListReturned.size());
        for(int i = 0; i < messageDTOListReturned.size(); i++){
            assertEquals("Name different from expected value.", expectedResult.get(i).getSenderId(), messageDTOListReturned.get(i).getSenderId());
            assertEquals("LastName different from expected value.", expectedResult.get(i).getReceiverId(), messageDTOListReturned.get(i).getReceiverId());
            assertEquals("Email different from expected value.", expectedResult.get(i).getMessage(), messageDTOListReturned.get(i).getMessage());
        }
    }

    private void checkMessageResult(MessageDTO messageResult, MessageDTO messageExpected) {
        assertNotNull("Result is null", messageResult);
        assertEquals("Name different from expected value.", messageExpected.getSenderId(), messageResult.getSenderId());
        assertEquals("LastName different from expected value.", messageExpected.getReceiverId(), messageResult.getReceiverId());
        assertEquals("Email different from expected value.", messageExpected.getMessage(), messageResult.getMessage());
    }

}
