package com.rjd.messenger.service;

import com.rjd.messenger.component.MyMessageSource;
import com.rjd.messenger.dto.FiltroUserDTO;
import com.rjd.messenger.dto.PagUserDTO;
import com.rjd.messenger.entity.User;
import com.rjd.messenger.exceptions.*;
import com.rjd.messenger.repository.UserRepository;
import com.rjd.messenger.service.user.UserService;
import com.rjd.messenger.service.user.UserServiceImpl;
import com.rjd.messenger.util.UserTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.*;

import static com.rjd.messenger.util.UserTestUtil.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    private static final String RESULT_LIST_IS_NULL = "Result list is null";
    private static final String RESULT_LIST_IS_NOT_NULL = "Result list is not null";
    private static final String RESULT_LIST_IS_EMPTY = "Result list is empty";
    private static final String RESULT_LIST_SIZE_NOT_EXPECTED = "Result list size is not expected";
    private static final String RESULT_IS_NOT_NULL = "result is not null";


    private MyMessageSource myMessageSource = mock(MyMessageSource.class);

    private UserRepository userRepository = mock(UserRepository.class);

    private UserService userService;

    private User userResult;

    @Before
    public void setUp(){
        userService = new UserServiceImpl(myMessageSource, userRepository);
        userResult = null;
    }

    @Test
    public void testFindAllSuccess() throws SQLException {
        Pageable page = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(Arrays.asList(getUserAdmin(), getUserComum()));
        when(userRepository.findByIdNot("", page)).thenReturn(userPage);
        PagUserDTO pagUserDTO = userService.findAll("0","10");
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_EMPTY, !pagUserDTO.getUsersList().isEmpty());
        assertEquals(RESULT_LIST_SIZE_NOT_EXPECTED, 2, pagUserDTO.getUsersList().size());
        checkUserTest(getUserAdmin(), pagUserDTO.getUsersList().get(0));
        checkUserTest(getUserComum(), pagUserDTO.getUsersList().get(1));
        verify(userRepository, times(1)).findByIdNot("", page);
    }

    @Test
    public void testFindAllEmptySuccess() throws SQLException {
        Pageable page = PageRequest.of(0, 10);
        when(userRepository.findByIdNot("", page)).thenReturn(new PageImpl<>(new ArrayList<>()));
        PagUserDTO pagUserDTO = userService.findAll("0","10");
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_NOT_NULL, pagUserDTO.getUsersList().isEmpty());
        verify(userRepository, times(1)).findByIdNot("", page);
    }

    @Test(expected = InvalidFieldException.class)
    public void testFindAllFailNuPage() {
        when(myMessageSource.get("msg.warn.field.must.be.integer")).thenReturn("Field must be Integer:");
        userService.findAll("a","10");
    }

    @Test(expected = InvalidFieldException.class)
    public void testFindAllFailPageSize() {
        when(myMessageSource.get("msg.warn.field.must.be.integer")).thenReturn("Field must be Integer:");
        userService.findAll("0","a");
    }

    @Test
    public void testFindAllWithFilterNameLastNameSuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),"0","10");
        Pageable page = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(Arrays.asList(getUserAdmin(), getUserComum()));
        when(userRepository.findByIdNotAndNameLikeAndLastNameLike("", filtroUserDTO.getName(), filtroUserDTO.getLastName(), page)).thenReturn(userPage);
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_EMPTY, !pagUserDTO.getUsersList().isEmpty());
        assertEquals(RESULT_LIST_SIZE_NOT_EXPECTED, 2, pagUserDTO.getUsersList().size());
        checkUserTest(getUserAdmin(), pagUserDTO.getUsersList().get(0));
        checkUserTest(getUserComum(), pagUserDTO.getUsersList().get(1));
        verify(userRepository, times(1)).findByIdNotAndNameLikeAndLastNameLike("", filtroUserDTO.getName(), filtroUserDTO.getLastName(), page);
    }

    @Test
    public void testFindAllWithFilterNameSuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), null,"0","10");
        Pageable page = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(Arrays.asList(getUserAdmin(), getUserComum()));
        when(userRepository.findByIdNotAndNameLike("", filtroUserDTO.getName(), page)).thenReturn(userPage);
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_EMPTY, !pagUserDTO.getUsersList().isEmpty());
        assertEquals(RESULT_LIST_SIZE_NOT_EXPECTED, 2, pagUserDTO.getUsersList().size());
        checkUserTest(getUserAdmin(), pagUserDTO.getUsersList().get(0));
        checkUserTest(getUserComum(), pagUserDTO.getUsersList().get(1));
        verify(userRepository, times(1)).findByIdNotAndNameLike("", filtroUserDTO.getName(), page);
    }

    @Test
    public void testFindAllWithFilterLastNameSuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(null, UserTestUtil.getUserAdmin().getLastName(),"0","10");
        Pageable page = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(Arrays.asList(getUserAdmin(), getUserComum()));
        when(userRepository.findByIdNotAndLastNameLike("", filtroUserDTO.getLastName(), page)).thenReturn(userPage);
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_EMPTY, !pagUserDTO.getUsersList().isEmpty());
        assertEquals(RESULT_LIST_SIZE_NOT_EXPECTED, 2, pagUserDTO.getUsersList().size());
        checkUserTest(getUserAdmin(), pagUserDTO.getUsersList().get(0));
        checkUserTest(getUserComum(), pagUserDTO.getUsersList().get(1));
        verify(userRepository, times(1)).findByIdNotAndLastNameLike("", filtroUserDTO.getLastName(), page);
    }

    @Test
    public void testFindAllWithFilterSuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(null, null,"0","10");
        Pageable page = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(Arrays.asList(getUserAdmin(), getUserComum()));
        when(userRepository.findByIdNot("", page)).thenReturn(userPage);
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_EMPTY, !pagUserDTO.getUsersList().isEmpty());
        assertEquals(RESULT_LIST_SIZE_NOT_EXPECTED, 2, pagUserDTO.getUsersList().size());
        checkUserTest(getUserAdmin(), pagUserDTO.getUsersList().get(0));
        checkUserTest(getUserComum(), pagUserDTO.getUsersList().get(1));
        verify(userRepository, times(1)).findByIdNot("", page);
    }

    @Test
    public void testFindAllWithFilterNameLastNameEmptySuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),"0","10");
        Pageable page = PageRequest.of(0, 10);
        when(userRepository.findByIdNotAndNameLikeAndLastNameLike("", filtroUserDTO.getName(), filtroUserDTO.getLastName(), page)).thenReturn(new PageImpl<>(new ArrayList<>()));
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_NOT_NULL, pagUserDTO.getUsersList().isEmpty());
        verify(userRepository, times(1)).findByIdNotAndNameLikeAndLastNameLike("", filtroUserDTO.getName(), filtroUserDTO.getLastName(), page);
    }

    @Test
    public void testFindAllWithFilterNameEmptySuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), null,"0","10");
        Pageable page = PageRequest.of(0, 10);
        when(userRepository.findByIdNotAndNameLike("",UserTestUtil.getUserAdmin().getName(), page)).thenReturn(new PageImpl<>(new ArrayList<>()));
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_NOT_NULL, pagUserDTO.getUsersList().isEmpty());
        verify(userRepository, times(1)).findByIdNotAndNameLike("",UserTestUtil.getUserAdmin().getName(), page);
    }

    @Test
    public void testFindAllWithFilterLastNameEmptySuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(null, UserTestUtil.getUserAdmin().getLastName(),"0","10");
        Pageable page = PageRequest.of(0, 10);
        when(userRepository.findByIdNotAndLastNameLike("",UserTestUtil.getUserAdmin().getLastName(), page)).thenReturn(new PageImpl<>(new ArrayList<>()));
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_NOT_NULL, pagUserDTO.getUsersList().isEmpty());
        verify(userRepository, times(1)).findByIdNotAndLastNameLike("",UserTestUtil.getUserAdmin().getLastName(), page);
    }

    @Test
    public void testFindAllWithFilterEmptySuccess() throws SQLException {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(null, null,"0","10");
        Pageable page = PageRequest.of(0, 10);
        when(userRepository.findByIdNot("", page)).thenReturn(new PageImpl<>(new ArrayList<>()));
        PagUserDTO pagUserDTO = userService.findAll(filtroUserDTO);
        assertNotNull(RESULT_LIST_IS_NULL, pagUserDTO.getUsersList());
        assertTrue(RESULT_LIST_IS_NOT_NULL, pagUserDTO.getUsersList().isEmpty());
        verify(userRepository, times(1)).findByIdNot("", page);
    }

    @Test(expected = InvalidFieldException.class)
    public void testFindAllWithFilterFailNuPage() {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),"a","10");
        when(myMessageSource.get("msg.warn.field.must.be.integer")).thenReturn("Field must be Integer:");
        userService.findAll(filtroUserDTO);
    }

    @Test(expected = InvalidFieldException.class)
    public void testFindAllWithFilterFailPageSize() {
        FiltroUserDTO filtroUserDTO = new FiltroUserDTO(UserTestUtil.getUserAdmin().getName(), UserTestUtil.getUserAdmin().getLastName(),"0","a");
        when(myMessageSource.get("msg.warn.field.must.be.integer")).thenReturn("Field must be Integer:");
        userService.findAll(filtroUserDTO);
    }

    @Test
    public void testFindByIdSuccess() {
        Optional<User> optionalUsuario = Optional.of(getUserAdmin());
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenReturn(optionalUsuario);
        userResult = userService.findById(UserTestUtil.ID_USER_ADMIN);
        checkUserTest(getUserAdmin(), userResult);
        verify(userRepository, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testFindByIdFailNaoEncontrado() {
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenThrow(new NoSuchElementException());
        when(myMessageSource.get("msg.warn.user.not.found")).thenReturn("User not found in database with id:");
        userService.findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test
    public void testCreateSuccess() {
        User user = getUserAdmin();
        when(userRepository.save(user)).thenReturn(user);
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenThrow(new NoSuchElementException());
        userResult = userService.create(user);
        checkUserTest(getUserAdmin(), userResult);
        verify(userRepository, times(1)).save(user);
        verify(userRepository, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test(expected = EntityAlreadyRegisteredException.class)
    public void testCreateFailUsuarioJaCadastrado() {
        Optional<User> usuarioOptional = Optional.of(getUserAdmin());
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenReturn(usuarioOptional);
        when(myMessageSource.get("msg.warn.user.already.registered")).thenReturn("User already exists in database.");
        userResult = userService.create(getUserAdmin());
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(userRepository, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
        verify(myMessageSource, times(2)).get("msg.warn.user.already.registered");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailIdNull() {
        User usuario = getUserAdmin();
        usuario.setId(null);
        when(myMessageSource.get("msg.warn.id.mandatory")).thenReturn("Field 'id' is mandatory.");
        userResult = userService.create(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.id.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailNameNull() {
        User usuario = getUserAdmin();
        usuario.setName(null);
        when(myMessageSource.get("msg.warn.name.mandatory")).thenReturn("Field 'name' is mandatory.");
        userResult = userService.create(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.name.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailLastNameNull() {
        User usuario = getUserAdmin();
        usuario.setLastName(null);
        when(myMessageSource.get("msg.warn.last.name.mandatory")).thenReturn("Field 'lastName' is mandatory.");
        userResult = userService.create(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.last.name.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailEmailNull() {
        User usuario = getUserAdmin();
        usuario.setEmail(null);
        when(myMessageSource.get("msg.warn.email.mandatory")).thenReturn("Field 'email' is mandatory.");
        userResult = userService.create(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.email.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailPasswordNull() {
        User usuario = getUserAdmin();
        usuario.setPassword(null);
        when(myMessageSource.get("msg.warn.password.mandatory")).thenReturn("Field 'password' is mandatory.");
        userResult = userService.create(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.password.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testCreateFailUserRoleNull() {
        User usuario = getUserAdmin();
        usuario.setUserRole(null);
        when(myMessageSource.get("msg.warn.user.role.mandatory")).thenReturn("Field 'userRole' is mandatory.");
        userResult = userService.create(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.user.role.mandatory");
    }

    @Test
    public void testUpdateSuccess() {
        User usuario = getUserAdmin();
        when(userRepository.save(usuario)).thenReturn(usuario);
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenReturn(Optional.of(usuario));
        userResult = userService.update(usuario);
        checkUserTest(getUserAdmin(), userResult);
        verify(userRepository, times(1)).save(usuario);
        verify(userRepository, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void testUpdateFail404NotFound() {
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenThrow(new NoSuchElementException());
        when(myMessageSource.get("msg.warn.user.not.found")).thenReturn("User not found in database with id:");
        userResult = userService.update(getUserAdmin());
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(userRepository, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
        verify(myMessageSource, times(2)).get("msg.warn.user.not.found");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailIdNull()  {
        User usuario = getUserAdmin();
        usuario.setId(null);
        when(myMessageSource.get("msg.warn.id.mandatory")).thenReturn("Field 'id' is mandatory.");
        userResult = userService.update(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.id.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailNameNull() {
        User usuario = getUserAdmin();
        usuario.setName(null);
        when(myMessageSource.get("msg.warn.name.mandatory")).thenReturn("Field 'name' is mandatory.");
        userResult = userService.update(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.name.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailLastNameNull() {
        User usuario = getUserAdmin();
        usuario.setLastName(null);
        when(myMessageSource.get("msg.warn.last.name.mandatory")).thenReturn("Field 'lastName' is mandatory.");
        userResult = userService.update(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.last.name.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailEmailNull() {
        User usuario = getUserAdmin();
        usuario.setEmail(null);
        when(myMessageSource.get("msg.warn.email.mandatory")).thenReturn("Field 'email' is mandatory.");
        userResult = userService.update(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.email.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailPasswordNull() {
        User usuario = getUserAdmin();
        usuario.setPassword(null);
        when(myMessageSource.get("msg.warn.password.mandatory")).thenReturn("Field 'password' is mandatory.");
        userResult = userService.update(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.password.mandatory");
    }

    @Test(expected = MandatoryFieldException.class)
    public void testUpdateFailUserRoleNull() {
        User usuario = getUserAdmin();
        usuario.setUserRole(null);
        when(myMessageSource.get("msg.warn.user.role.mandatory")).thenReturn("Field 'userRole' is mandatory.");
        userResult = userService.update(usuario);
        assertNull(RESULT_IS_NOT_NULL, userResult);
        verify(myMessageSource, times(2)).get("msg.warn.user.role.mandatory");
    }

    @Test
    public void testDeleteSuccess() {
        User usuario = getUserAdmin();
        Optional<User> usuarioOptional = Optional.of(usuario);
        doNothing().when(userRepository).deleteById(UserTestUtil.ID_USER_ADMIN);
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenReturn(usuarioOptional).thenThrow(new NoSuchElementException());
        userService.deleteById(UserTestUtil.ID_USER_ADMIN);
        verify(userRepository, times(1)).deleteById(UserTestUtil.ID_USER_ADMIN);
        verify(userRepository, times(2)).findById(UserTestUtil.ID_USER_ADMIN);
    }

    @Test(expected = RegistryNotFoundException.class)
    public void tesDeleteFailUserNotFound() {
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenThrow(new NoSuchElementException());
        when(myMessageSource.get("msg.warn.user.not.found")).thenReturn("User not found in database with id:");
        userService.deleteById(UserTestUtil.ID_USER_ADMIN);
        verify(userRepository, times(1)).findById(UserTestUtil.ID_USER_ADMIN);
        verify(myMessageSource, times(2)).get("msg.warn.user.not.found");
    }

    @Test(expected = RegistryNotDeletedException.class)
    public void testDeleteFailDataNotDeletedException() {
        User usuario = getUserAdmin();
        Optional<User> usuarioOptional = Optional.of(usuario);
        doNothing().when(userRepository).deleteById(UserTestUtil.ID_USER_ADMIN);
        when(userRepository.findById(UserTestUtil.ID_USER_ADMIN)).thenReturn(usuarioOptional).thenReturn(usuarioOptional);
        when(myMessageSource.get("msg.warn.error.delete.user")).thenReturn("Error while trying to delete user.");
        userService.deleteById(UserTestUtil.ID_USER_ADMIN);
        verify(userRepository, times(1)).deleteById(UserTestUtil.ID_USER_ADMIN);
        verify(userRepository, times(2)).findById(UserTestUtil.ID_USER_ADMIN);
        verify(myMessageSource, times(2)).get("msg.warn.error.delete.user");
    }

    private void checkUserTest(User userExpected,User userResult) {
        assertNotNull("Result is null", userResult);
        assertEquals("Id different from expected value.", userExpected.getId(), userResult.getId());
        assertEquals("Name different from expected value.", userExpected.getName(), userResult.getName());
        assertEquals("LastName different from expected value.", userExpected.getLastName(), userResult.getLastName());
        assertEquals("Email different from expected value.", userExpected.getEmail(), userResult.getEmail());
        assertEquals("Password different from expected value.", userExpected.getPassword(), userResult.getPassword());
        assertEquals("UserRole different from expected value.", userExpected.getUserRole(), userResult.getUserRole());
    }
}
