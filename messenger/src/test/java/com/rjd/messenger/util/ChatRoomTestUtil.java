package com.rjd.messenger.util;

import com.rjd.messenger.entity.ChatRoom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatRoomTestUtil {

    public static final String ID_CHAT_ROOM = "629ef3b4-64cc-4f67-862e-a95879686a4d";

    public static ChatRoom getChatRoom() {
        return new ChatRoom(ID_CHAT_ROOM, "Sala Teste", Arrays.asList("fulanoAdmin", "fulanoAuditor"));
    }

    public static List<ChatRoom> getChatRoomList() {
        return Arrays.asList(new ChatRoom("629ef3b4-64cc-4f67-862e-a95879686a4d", "Sala Teste", Arrays.asList("fulanoAdmin", "fulanoAuditor")),
                new ChatRoom("b3f09f3e-4ec8-4b58-85fb-440f3d1ce30a", "Sala Teste 2", Arrays.asList("fulanoAdmin", "fulanoAuditor", "fulanoComun")));
    }
}
