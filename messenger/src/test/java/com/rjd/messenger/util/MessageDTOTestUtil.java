package com.rjd.messenger.util;

import com.rjd.messenger.dto.FiltroMessageDTO;
import com.rjd.messenger.dto.MessageDTO;
import com.rjd.messenger.entity.Message;
import org.bson.types.ObjectId;

import java.util.Date;

import static com.rjd.messenger.util.UserTestUtil.*;

public class MessageDTOTestUtil {

    public static MessageDTO getMessageDTOWithDateAndIdNull(){
        return new MessageDTO(ID_USER_ADMIN, ID_USER_ADMIN, ID_USER_COMUM, ID_USER_COMUM, "mensagem teste");
    }

    public static MessageDTO getMessageDTO(){
        MessageDTO message = new MessageDTO(ID_USER_ADMIN, ID_USER_ADMIN, ID_USER_COMUM, ID_USER_COMUM, "mensagem teste");
        message.setDtMessageLongStr(Long.valueOf(new Date(2018,2,18).getTime()).toString());
        message.setId("507f1f77bcf86cd799439011");
        return message;
    }

    public static FiltroMessageDTO getFiltroMessageDTO(){
        return new FiltroMessageDTO(ID_USER_ADMIN, ID_USER_COMUM, "0", "10");
    }
}
