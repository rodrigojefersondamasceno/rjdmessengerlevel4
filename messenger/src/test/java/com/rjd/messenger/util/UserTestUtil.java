package com.rjd.messenger.util;

import com.rjd.messenger.entity.User;

import java.util.Arrays;
import java.util.List;

public class UserTestUtil {

    public static final String ID_USER_ADMIN = "usrTest1";
    public static final String ID_USER_COMUM = "usrTest2";

    public static User getUserAdmin(){
        return new User(ID_USER_ADMIN, "Usuario1", "Silva1", "usrTest1@gmail.com", "12345", "ADMIN");
    }

    public static User getUserComum(){
        return new User("usrTest2", "Usuario2", "Silva2", "usrTest2@gmail.com", "67890", "COMUM");
    }

    public static List<User> getUserList(){
        return Arrays.asList(getUserAdmin(),getUserComum());
    }
}
