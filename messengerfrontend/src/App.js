import React, { Component } from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Aux from './hoc/Auxiliary';
import withClass from './hoc/withClass';
import Layout from './containers/Layout/Layout';
import MainScreen from './containers/MainScreen/MainScreen';
import LoginPage from './components/Login/LoginPage';
import Logout from './components/Logout/Logout';
import MessageSearchScreen from './containers/MessageSearchScreen/MessageSearchScreen'
import * as constants from './shared/constantes';


import styles from './App.css';

class App extends Component {
  render() {
    let routes = (
      <Switch>
        <Route path='/login' component={LoginPage} />
        <Redirect to='/login' />
      </Switch>
    );

    if (this.props.isAuthenticated && this.props.userRole === constants.ROLE_AUDITOR) {
      routes = (
        <Switch>
          <Route path='/main' component={MainScreen} />
          <Route path='/messages' component={MessageSearchScreen}/>
          <Route path='/logout' component={Logout}/>
          <Redirect to='/main' />
        </Switch>
      );
    } else if (this.props.isAuthenticated && this.props.userRole !== constants.ROLE_AUDITOR) {
      routes = (
        <Switch>
          <Route path='/main' component={MainScreen} />
          <Route path='/logout' component={Logout}/>
          <Redirect to='/main' />
        </Switch>
      );
    }

    return (
      <Aux>
        <Layout>
          {routes}
        </Layout>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    userRole: state.auth.userRole
  }
}

export default withRouter(connect(mapStateToProps)(withClass(App, styles.App)));
