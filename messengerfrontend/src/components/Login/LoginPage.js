import React, {Component} from 'react';
import Button from '../UI/Button/Button';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';

import styles from './LoginPage.css';

class LoginPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            userId: '',
            password: '',
            error: ''
        };
    }

    handleUserIdChange = (event) => {
        this.setState({userId: event.target.value});
    }

    handlePasswordChange = (event) => {
        this.setState({password: event.target.value})
    }
    
    submitHandler = (event) => {
        event.preventDefault();
        this.props.onAuth(this.state.userId, this.state.password);
    }

    render(){
        let errorH3 = null;
        if(this.props.errorMessage){
            errorH3 = (
                <h3 className={styles.ErrorMessage}>{this.props.errorMessage}</h3>
            )
        }
        return(
            <form onSubmit={this.submitHandler}>
                <h1>RJD Messenger Login Page</h1>
                {errorH3}
                <div className={styles.InputDiv}>
                    <label className={styles.Label}>User Name: </label>
                    <input className={styles.Input} type="text" value={this.state.userId} 
                        onChange={this.handleUserIdChange} placeholder="User Name"/>
                </div>
                <br/>
                <div className={styles.InputDiv}>
                    <label className={styles.Label}>Password: </label>
                    <input className={styles.Input} type="password" value={this.state.password} 
                        onChange={this.handlePasswordChange} placeholder="Password"/>
                </div>
                <br/>
                <Button disabled={this.state.userId === '' || this.state.password === ''}>Log In</Button>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onAuth: (userId, password) => dispatch(actions.auth(userId, password))
    }
}

const mapStateToProps = state => {
    return{
        errorMessage: state.auth.error
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);