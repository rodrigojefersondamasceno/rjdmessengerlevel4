import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux'

import * as actions from '../../store/actions/index';

class Logout extends Component{
    componentDidMount(){
        if(this.props.isAuthenticated){
            this.props.onLogout();
        }
    }
    render(){
        let logout = null;
        if(!this.props.isAuthenticated){
            logout = <Redirect to="/"/>; 
        }
        return logout;
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onLogout: () => dispatch(actions.logout())
    }
}

const mapStateToProps = state => {
    return{
        isAuthenticated: state.auth.isAuthenticated
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);