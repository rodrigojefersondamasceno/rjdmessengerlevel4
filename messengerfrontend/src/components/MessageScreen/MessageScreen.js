import React, {Component} from "react";
import { connect } from 'react-redux';
import SockJsClient from 'react-stomp';

import styles from "./MessageScreen.css";

import Messages from "../Messages/Messages";
import MessageScreenHead from './MessageScreenHead/MessageScreenHead'
import MessageScreenFoot from "./MessageScreenFoot/MessageScreenFoot";

import * as actions from '../../store/actions/index';

class MessageScreen extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            messageToSend:"",
            messages:[]
        }
    }


    onClickSendHandler = () => {
        const message = this.addMessage();
        const urlToSent = '/user/' + this.props.idSelectedUser + '/queue/listen';
        this.props.onSendMessage(message, this.clientRef, urlToSent);
        this.setState({messageToSend:''});
    }

    addMessage = () => {
        const messageToSend = this.state.messageToSend;
        const message = {
            key: this.props.messageList.length,
            senderName:this.props.userName,
            senderId: this.props.userId,
            receiverName: this.props.nameSelectedUser,
            receiverId: this.props.idSelectedUser,
            message: messageToSend
        }
        return message;
    }

    onChangeMessageArea = (event) => {
        this.setState({messageToSend:event.target.value});
    }

    receiveMessageHandler = (message) => {
        this.props.onSelectUser(message.senderId, message.senderName);
        this.props.onRecoverConversation(message.senderId, message.receiverId);   
        this.props.onReceiveMessage(message);
    }

    render(){
        return(
            <div className={styles.MessageScreen}>
                <MessageScreenHead name={this.props.nameSelectedUser}/>
                <Messages messages={this.props.messageList} userLoged={this.props.userName}/>
                <MessageScreenFoot
                    changeTextArea={this.onChangeMessageArea} 
                    valueTextArea={this.state.messageToSend}
                    clickSendButton={this.onClickSendHandler}
                    clickSendAnswer={this.onClickAnswerHandler}
                    disabledSendButton={this.props.nameSelectedUser === null || this.state.messageToSend === ''}/>
                <SockJsClient url="http://localhost:8080/messenger" 
                    topics={['/user/queue/listen']} 
                    onMessage={(message) => {this.receiveMessageHandler(message)}}
                    ref={(client) => {this.clientRef = client}}/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        messageList: state.message.messageList,
        nameSelectedUser: state.user.nameSelectedUser,
        idSelectedUser: state.user.idSelectedUser,
        userName: state.auth.userName,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onSendMessage: (message, clientRef, urlToSent) => dispatch(actions.sendMessage(message, clientRef, urlToSent)),
        onReceiveMessage: (message) => dispatch(actions.receiveMessage(message)),
        onSelectUser: (idSelectedUser, nameSelectedUser) => dispatch(actions.selectUser(idSelectedUser, nameSelectedUser)),
        onRecoverConversation: (idSelectedUser, idLoggedUser) => dispatch(actions.recoverConversation(idSelectedUser, idLoggedUser))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageScreen);