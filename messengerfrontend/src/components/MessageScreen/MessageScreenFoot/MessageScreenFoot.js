import React from 'react';
import Button from '../../UI/Button/Button';

import styles from './MessageScreenFoot.css'

export const MessageScreenFoot = (props) => {
    return (
        <div className={styles.Foot}>
            <textarea rows="3" onChange={props.changeTextArea} value={props.valueTextArea} />
            <div className={styles.FootButtonArea} align="right">
                <Button 
                    clicked={props.clickSendButton}
                    disabled={props.disabledSendButton}>Send</Button>
            </div>
        </div>
    );
}

export default MessageScreenFoot;