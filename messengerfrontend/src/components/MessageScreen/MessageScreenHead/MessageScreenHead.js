import React from 'react';

import styles from './MessageScreenHead.css';

const messageScreenHead = (props) => {
    return (
        <div className={styles.MessageScreenHead}>
            <p>{props.name}</p>
        </div>
    )
}

export default messageScreenHead;