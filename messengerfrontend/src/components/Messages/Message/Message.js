import React, {Component} from 'react';
import PropTypes from 'prop-types';

import styles from "./Message.css"

export class Message extends Component {

    componentDidMount(){
        this.resizeMessageAcordingToText();
    }

    resizeMessageAcordingToText = () => {
        var messageTextArea = this.refs.messageTextArea;
        messageTextArea.style.height = messageTextArea.scrollHeight + 'px';
    }

    render(){
        let classCSS = styles.MessageAudit;
        if(this.props.userLoged && this.props.userLoged === this.props.sender){
            classCSS = styles.MessageSent;
        }else if (this.props.userLoged && this.props.userLoged === this.props.receiver){
            classCSS = styles.MessageReceived;
        }

        if(this.props.resizeMessageAcordingToText){
            this.resizeMessageAcordingToText = this.props.resizeMessageAcordingToText;
        }
        return (
            <div className={classCSS}>
                <p className={styles.MessageInfo}>{this.props.sender} at {this.props.dtMessage} to {this.props.receiver}</p>
                <textarea ref="messageTextArea" id="messageTextArea" className={styles.MessageText} readOnly value={this.props.message}/> 
            </div>
        )
    }
}

Message.propTypes = {
    sender: PropTypes.string,
    dtMessage: PropTypes.string,
    receiver: PropTypes.string,
    message: PropTypes.string
}


export default Message;