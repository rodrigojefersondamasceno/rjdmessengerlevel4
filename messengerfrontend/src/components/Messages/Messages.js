import React, {Component} from 'react';

import styles from './Messages.css'

import Message from './Message/Message';

export class Messages extends Component{

    componentDidMount(){
        this.scrollDownMessages();
    }

    componentDidUpdate(){
        this.scrollDownMessages();
    }

    scrollDownMessages = () => {
        const element = document.getElementById("messages");
        element.scrollTop = element.offsetTop + element.offsetHeight;
    }

    render(){

        if(this.props.scrollDownMessages){
            this.scrollDownMessages = this.props.scrollDownMessages;
        }

        let messages = this.props.messages.map((message, index) => {
            const dtMessageLong = Number.parseInt(message.dtMessageLongStr, 10);
            return <Message
                index={index}
                key={index}
                id={message.id}
                message={message.message}
                dtMessage={new Date(dtMessageLong).toLocaleString()}
                sender={message.senderName}
                receiver={message.receiverName}
                userLoged={this.props.userLoged}
                />
            });

        return (
            <div className={styles.Messages} id="messages">
                {messages}
            </div>
        );
    }
}

export default Messages;