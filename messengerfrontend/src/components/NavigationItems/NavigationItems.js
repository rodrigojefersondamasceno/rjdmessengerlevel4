import React, { Component } from 'react';
import { connect } from 'react-redux';

import styles from './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';
import * as constants from '../../shared/constantes';


export class NavigationItems extends Component {
    render() {
        let navigationItens = (
            <ul className={styles.NavigationItems}>
                <NavigationItem link='/login'>Login</NavigationItem>
            </ul>
        );

        if (this.props.isAuthenticated && this.props.userRole === constants.ROLE_AUDITOR) {
            navigationItens = (
                <ul className={styles.NavigationItems}>
                    <NavigationItem link='/main'>Main</NavigationItem>
                    <NavigationItem link='/messages'>Messages</NavigationItem>
                    <NavigationItem link='/logout'>Logout</NavigationItem>
                </ul>
            )
        } else if (this.props.isAuthenticated && this.props.userRole !== constants.ROLE_AUDITOR) {
            navigationItens = (
                <ul className={styles.NavigationItems}>
                    <NavigationItem link='/main'>Main</NavigationItem>
                    <NavigationItem link='/logout'>Logout</NavigationItem>
                </ul>
            )
        }

        return navigationItens;
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        userRole: state.auth.userRole
    }
}

export default connect(mapStateToProps)(NavigationItems);