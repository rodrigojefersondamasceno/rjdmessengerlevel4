import React from 'react';

import styles from './Toolbar.css';
import NavigationItems from '../NavigationItems';


const toolbar = (props) => {
    let usuario = null;
    if(props.isAuthenticated) {
        usuario = <div className={styles.userName}>
                    <p>Olá {props.userName}</p>
                  </div>
    }
    return (
        <header className={styles.Toolbar}>
            <NavigationItems />
            {usuario}
        </header>
    );
}

export default toolbar;

