import React from 'react';

import styles from './Button.css';

const button = (props) => {
    return (<button style={{width:props.width}}
        className={styles.Button}
        onClick={props.clicked}
        disabled={props.disabled}>{props.children}</button>
    )
};

export default button;