import React, { Component } from 'react';

import styles from './Pagination.css';

class Pagination extends Component {
    incrementPageHandler = () => {
        const proxPagAtual = Number.parseInt(this.props.pagAtual, 10) + 1;
        if (this.props.changePageHandler) {
            this.props.changePageHandler(proxPagAtual);
        }
    }

    decrementPageHandler = () => {
        const proxPagAtual = Number.parseInt(this.props.pagAtual, 10) - 1;
        if (this.props.changePageHandler) {
            this.props.changePageHandler(proxPagAtual);
        }
    }

    render() {
        return (
            <div className={styles.Pagination} style={{height:this.props.height}}>
                <button
                    className={styles.PagButton}
                    onClick={() => this.decrementPageHandler()}
                    disabled={this.props.pagAtual <= 1}>ant</button>
                <span>{this.props.pagAtual} / {this.props.nuMaxPag}</span>
                <button
                    className={styles.PagButton}
                    onClick={() => this.incrementPageHandler()}
                    disabled={this.props.pagAtual === this.props.nuMaxPag || this.props.nuMaxPag <= 1}>prox</button>
            </div>
        )
    }
}

export default Pagination;