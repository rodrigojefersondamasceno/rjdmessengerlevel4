import React from 'react';
import styles from  "./User.css"

const user = (props) =>{
    let userStyle = styles.User;
    if(props.isSelected){
        userStyle = styles.UserSelected
    }
    return (
        <div className={userStyle} onClick={props.clicked}>
            <p>{props.name} {props.lastName}</p>
        </div>
    );
}

export default user;