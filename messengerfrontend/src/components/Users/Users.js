import React, { Component } from 'react';
import User from './User/User';
import UsersSearch from './UsersSearch/UsersSearch';
import { connect } from 'react-redux';
import Pagination from '../UI/Pagination/Pagination';

import styles from './Users.css'
import * as actions from '../../store/actions/index';

export class Users extends Component {
    componentDidMount() {
        if (this.props.isAuthenticated) {
            this.props.onUsersFind(1, this.props.nameFilter, this.props.lastNameFilter);
        }
    }

    selectUserHandler = (selectedUserId, selectedUserName) => {
        this.props.onSelectUser(selectedUserId, selectedUserName);
        this.props.onRecoverConversation(selectedUserId, this.props.idLoggedUser);
    }

    changePageHandler = (nuProxPag) => {
        this.props.onUsersFind(nuProxPag, this.props.nameFilter, this.props.lastNameFilter);
    }

    render() {
        let userList = null;
        if (this.props.usersList) {
            userList = this.props.usersList.map((user, index) => {
                return <User
                    index={index}
                    key={user.id}
                    name={user.name}
                    lastName={user.lastName}
                    id={user.id}
                    isSelected={user.id === this.props.idSelectedUser}
                    clicked={() => this.selectUserHandler(user.id, user.name)}
                />
            });
        }

        return (
            <div className={styles.Users}>
                <UsersSearch/>
                <div className={styles.UsersList}>
                    {userList}
                </div>
                <Pagination 
                    pagAtual={this.props.nuPageAtual} 
                    nuMaxPag={this.props.nuPageTotal}
                    changePageHandler={(nuProxPag) => this.changePageHandler(nuProxPag)}/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        usersList: state.user.usersList,
        nuPageAtual: state.user.nuPageAtual,
        nuPageTotal: state.user.nuPageTotal,
        idSelectedUser: state.user.idSelectedUser,
        nameFilter: state.user.nameFilter,
        lastNameFilter: state.user.lastNameFilter,
        isAuthenticated: state.auth.isAuthenticated,
        idLoggedUser: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUsersFind: (nuPage, nameFilter, lastNameFilter) => dispatch(actions.usersFind(nuPage, nameFilter, lastNameFilter)),
        onSelectUser: (idSelectedUser, nameSelectedUser) => dispatch(actions.selectUser(idSelectedUser, nameSelectedUser)),
        onRecoverConversation: (idSelectedUser, idLoggedUser) => dispatch(actions.recoverConversation(idSelectedUser, idLoggedUser))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);