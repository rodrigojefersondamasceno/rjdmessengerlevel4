import React, { Component } from 'react';
import Button from '../../UI/Button/Button';
import { connect } from 'react-redux';

import styles from './UsersSearch.css';
import * as actions from '../../../store/actions/index';

export class UserSearch extends Component {
    cleanHandler = () => {
        this.setState({nameSearch: "",
                        lastNameSearch: ""});
        this.props.onChangeNameFilter('');
        this.props.onChangeLastNameFilter('');
        this.props.onUsersFind(1, '', '');
    }

    searchHandler = () => {
        this.props.onUsersFind(1, this.props.nameFilter, this.props.lastNameFilter);
    }

    onChangeNameHandler = (event) => {
        this.props.onChangeNameFilter(event.target.value);
    }

    onChangeLastNameHandler = (event) => {
        this.props.onChangeLastNameFilter(event.target.value);
    }

    render() {
        const disabled = this.props.nameFilter === "" && this.props.lastNameFilter === "";
        return (
            <div className={styles.UsersSearch}>
                <div className={styles.InputArea}>
                    <input
                        onChange={this.onChangeNameHandler}
                        className={styles.FiltroInput} 
                        placeholder="Name" 
                        value={this.props.nameFilter}/>
                    <input
                        onChange={this.onChangeLastNameHandler}
                        className={styles.FiltroInput} 
                        placeholder="Last name" 
                        value={this.props.lastNameFilter}/>
                </div>
                <div className={styles.ButtonArea}>
                    <Button 
                        clicked={this.cleanHandler}
                        disabled={disabled}>clean</Button>
                    <Button 
                        disabled={disabled}
                        clicked={this.searchHandler} width={90}>search</Button>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChangeNameFilter: (name) => dispatch(actions.onChangeNameFilter(name)),
        onChangeLastNameFilter: (lastName) => dispatch(actions.onChangeLastNameFilter(lastName)),
        onUsersFind: (nuPage, nameFilter, lastNameFilter) => dispatch(actions.usersFind(nuPage, nameFilter, lastNameFilter))
    }
}

const mapStateToProps = state => {
    return {
        nameFilter: state.user.nameFilter,
        lastNameFilter: state.user.lastNameFilter,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserSearch);