import React, {Component} from 'react';
import Aux from '../../hoc/Auxiliary';
import Toolbar from '../../components/NavigationItems/Toolbar/Toolbar';
import styles from './Layout.css';
import { connect } from 'react-redux';

class Layout extends Component{
    render(){
        return(
            <Aux>
                <Toolbar userName={this.props.userName} isAuthenticated={this.props.isAuthenticated}/>
                <main className={styles.Content}>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return{
        userName: state.auth.userName,
        isAuthenticated: state.auth.isAuthenticated
    }
}

export default connect(mapStateToProps)(Layout);