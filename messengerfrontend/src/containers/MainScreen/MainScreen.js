import React, { Component } from 'react';
import Users from '../../components/Users/Users';
import MessageScreen from '../../components/MessageScreen/MessageScreen';

import styles from './MainScreen.css';


class MainScreen extends Component {
    render() {
        return (
            <div className={styles.MainScreen}>
                <Users />
                <MessageScreen/>
            </div>
        );
    }
}

export default MainScreen;