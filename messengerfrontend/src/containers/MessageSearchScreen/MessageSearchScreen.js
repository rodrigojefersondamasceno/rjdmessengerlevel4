import React, {Component} from 'react';
import MessageSearchScreenHeader from './MessageSearchScreenHeader/MessageSearchScreenHeader'
import Messages from '../../components/Messages/Messages';
import Pagination from '../../components/UI/Pagination/Pagination';
import * as actions from '../../store/actions/index';

import { connect } from 'react-redux';

import styles from './MessageSearchScreen.css';

class MessageSearchScreen extends Component {
    changePageHandler = (nuProxPag) => {
        this.props.onMessagesAuditFind(nuProxPag, this.props.senderFilter, this.props.receiverFilter);
    }

    componentDidMount(){
        this.props.onMessagesAuditFind(1, this.props.senderFilter, this.props.receiverFilter);
    }
    
    render(){
        return (
            <div className={styles.MessageSearchScreen}>
                <MessageSearchScreenHeader/>
                <Messages messages={this.props.messageAuditList}/>
                <Pagination 
                    height={41}
                    pagAtual={this.props.nuPageAtual} 
                    nuMaxPag={this.props.nuPageTotal}
                    changePageHandler={(nuProxPag) => this.changePageHandler(nuProxPag)}/>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onMessagesAuditFind: (nuPage, sender, receiver) => dispatch(actions.onMessagesAuditFind(nuPage, sender, receiver))
    }
}

const mapStateToProps = state => {
    return {
        messageAuditList: state.message.messageAuditList,
        senderFilter: state.message.senderFilter,
        receiverFilter: state.message.receiverFilter,
        nuPageAtual: state.message.nuPageAtual,
        nuPageTotal: state.message.nuPageTotal
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageSearchScreen);