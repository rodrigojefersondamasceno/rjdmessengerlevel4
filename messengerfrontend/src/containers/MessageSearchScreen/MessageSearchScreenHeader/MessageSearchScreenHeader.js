import React, { Component } from 'react';
import Button from '../../../components/UI/Button/Button';
import * as actions from '../../../store/actions/index';

import styles from './MessageSearchScreenHeader.css';

import { connect } from 'react-redux';

class MessageSearchScreenHeader extends Component {
    
    cleanHandler = () => {
        this.props.onChangeSenderFilter('');
        this.props.onChangeReceiverFilter('');
        this.props.onMessagesAuditFind(1, '', '');
    }

    searchHandler = () => {
        this.props.onMessagesAuditFind(1, this.props.senderFilter, this.props.receiverFilter);
    }

    onChangeSenderHandler = (event) => {
        this.props.onChangeSenderFilter(event.target.value);
    }

    onChangeReceiverHandler = (event) => {
        this.props.onChangeReceiverFilter(event.target.value);
    }
    render() {
        const disabled = this.props.senderFilter === "" && this.props.receiverFilter === "";
        return (
            <div className={styles.MessageSearchScreenHeader}>
                <div className={styles.MainArea}>
                    <div className={styles.InputArea}>
                        <input
                            className={styles.FiltroInput}
                            value={this.props.senderFilter}
                            placeholder='Sender'
                            onChange={this.onChangeSenderHandler} />
                        <input
                            className={styles.FiltroInput}
                            value={this.props.receiverFilter}
                            placeholder='Receiver'
                            onChange={this.onChangeReceiverHandler} />
                    </div>
                    <div className={styles.ButtonArea}>
                        <Button
                            clicked={this.cleanHandler}
                            disabled={disabled}>clean</Button>
                        <Button
                            disabled={disabled}
                            clicked={this.searchHandler} width={90}>search</Button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChangeSenderFilter: (sender) => dispatch(actions.onChangeSenderFilter(sender)),
        onChangeReceiverFilter: (receiver) => dispatch(actions.onChangeReceiverFilter(receiver)),
        onMessagesAuditFind: (nuPage, sender, receiver) => dispatch(actions.onMessagesAuditFind(nuPage, sender, receiver))
    }
}

const mapStateToProps = state => {
    return {
        senderFilter: state.message.senderFilter,
        receiverFilter: state.message.receiverFilter
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageSearchScreenHeader);