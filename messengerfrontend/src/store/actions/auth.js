import * as actionTypes from './actionTypes';
import axios from '../../axios-instance';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (name, id, role) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        userName: name,
        userId: id,
        userRole: role
    }
}

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const logoutSuccess = () => {
    return {
        type: actionTypes.LOGOUT_SUCCESS
    }
}

export const logoutFail = (error) => {
    return {
        type: actionTypes.LOGOUT_FAIL,
        error: error
    }
}

export const logout = () => {
    return dispatch => {
        axios.get('/logout').then(response => {
            dispatch(logoutSuccess());
        }).catch(error => {
            dispatch(logoutFail(error));
        })
    }
}

export const auth = (userId, password) => {
    const authData = {
        username: userId,
        password: password
    };
    return dispatch => {
        axios.get('/user/' + userId, {
            auth: authData
        }).then(response => {
            if(response.data.name){
                dispatch(authSuccess(response.data.name, response.data.id, response.data.userRole));
            }else{
                dispatch(authFail('Invalid user name or passoword'));
            }
        }).catch(error => {
            dispatch(authFail(error));
        });        
    }
}