import * as actionTypes from './actionTypes';
import axios from '../../axios-instance';
import {updateObject} from '../../shared/utility';

export const sendMessage = (message, clientRef, urlToSent) => {
    const messageToBackend = {
        id: null,
        senderId: message.senderId,
        receiverId: message.receiverId,
        message: message.message
    }

    return dispatch => {
        axios.post('/message', messageToBackend).then(response => {
            dispatch(sendMessageSuccess(response.data));
            clientRef.sendMessage(urlToSent, JSON.stringify(response.data));
        }).catch(error => {
            dispatch(sendMessageFail(error));
        });
    }

}

export const receiveMessage = (message) => {
    const messageReceived = updateObject(message, {dtMessage: new Date(Number.parseInt(message.dtMessageLongStr,10))});
    return dispatch => {
        dispatch(sendMessageSuccess(messageReceived));
    }
}

export const sendMessageSuccess = (message) => {
    return {
        type: actionTypes.SEND_MESSAGE_SUCCESS,
        message: message
    }
}

export const sendMessageFail = (error) => {
    return {
        type: actionTypes.SEND_MESSAGE_FAIL,
        error: error
    }
}

export const recoverConversation = (idReceiver, idLoggedUser) => {
    const data = {
        senderId: idLoggedUser,
        receiverId: idReceiver
    }
    return dispatch => {
        axios.post('/message/chat', data).then(response => {
            dispatch(recoverConversationSuccess(response.data));
        }).catch(error => {
            dispatch(recoverConversationFail(error));
        })
    };
}

export const recoverConversationSuccess = (messageList) => {
    return{
        type: actionTypes.RECOVER_CONVERSATION_SUCCESS,
        messageList: messageList
    }
}

export const recoverConversationFail = (error) => {
    return {
        type: actionTypes.RECOVER_CONVERSATION_FAIL,
        error: error
    }
}

export const onMessagesAuditFind = (nuPage, sender, receiver) => {
    const nuProxPage = Number.parseInt(nuPage, 10) -1;
    const data = {
        senderId: sender,
        receiverId: receiver,
        nuPage: nuProxPage,
        pageSize: 10
    }
    return dispatch => {
        axios.post('/message/audit', data).then(response => {
            dispatch(onMessagesAuditFindSuccess(response.data));
        }).catch(error => {
            dispatch(onMessagesAuditFindFail(error));
        })
    }
}

export const onMessagesAuditFindFail = (error) => {
    return {
        type: actionTypes.MESSAGES_AUDIT_FIND_FAIL,
        error: error
    }
}

export const onMessagesAuditFindSuccess = (data) => {
    return {
        type: actionTypes.MESSAGES_AUDIT_FIND_SUCCESS,
        messageAuditList: data.messageDTOList, 
        nuPageAtual: data.nuPageAtual,
        nuPageTotal: data.nuPageTotal
    }
}

export const onChangeSenderFilter = (sender) => {
    return {
        type: actionTypes.CHANGE_SENDER_FILTER,
        sender: sender
    }
}

export const onChangeReceiverFilter = (receiver) => {
    return{
        type: actionTypes.CHANGE_RECEIVER_FILTER,
        receiver: receiver
    }
}