import * as actionTypes from './actionTypes';
import axios from '../../axios-instance';

export const usersInit = () => {
    return {
        type: actionTypes.USERS_INIT
    };
};

export const usersFindSuccess = (data) => {
    return {
        type: actionTypes.USERS_FIND_SUCCESS,
        usersList: data.usersList,
        nuPageAtual: data.nuPageAtual,
        nuPageTotal:data.nuPageTotal
    }
}

export const usersFindFail = (error) => {
    return {
        type: actionTypes.USERS_FIND_FAIL,
        error: error
    }
}

export const usersFind = (nuPage, nameFilter, lastNameFilter) => {
    const nuProxPage = Number.parseInt(nuPage, 10) -1;
    return dispatch => {
        const data = {
            nuPage: nuProxPage,
            pageSize: 10,
            name: nameFilter,
            lastName: lastNameFilter
            
        }
        dispatch(usersInit());
        axios.post("/user/filter", data).then(response => {
            dispatch(usersFindSuccess(response.data));
        }).catch(error => { 
            dispatch(usersFindFail(error));
        });
    };
};

export const selectUser = (idSelectedUser, nameSelectedUser) => {
    return {
        type: actionTypes.SELECT_USER,
        idSelectedUser: idSelectedUser,
        nameSelectedUser: nameSelectedUser
    }
};

export const onChangeNameFilter = (nameFilter) => {
    return {
        type: actionTypes.CHANGE_NAME_FILTER,
        nameFilter: nameFilter
    }
}

export const onChangeLastNameFilter = (lastNameFilter) => {
    return {
        type: actionTypes.CHANGE_LAST_NAME_FILTER,
        lastNameFilter: lastNameFilter
    }
}