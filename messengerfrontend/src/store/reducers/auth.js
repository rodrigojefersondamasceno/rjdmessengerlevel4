import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../../shared/utility';

const initialState = {
    userName: null,
    userId: null,
    userRole: null,
    isAuthenticated: false,
    error: null
}

const authStart = (state) => {
    return updateObject(state, {error: null});
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        userName: action.userName,
        userId: action.userId,
        userRole: action.userRole,
        isAuthenticated: true,
        error: null
    })
}

const authFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        userName: null,
        userId: null,
        userRole: null,
        isAuthenticated: false
    })
}

const logoutSuccess = (state) => {
    return updateObject(state, {
        userName: null,
        userId: null,
        userRole: null,
        isAuthenticated: false,
        error: null
    });
}

const logoutFail = (state, action) => {
    return updateObject(state, {
        error: action.error
    })
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.AUTH_START:
            return authStart(state);
        case actionTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.LOGOUT_SUCCESS:
            return logoutSuccess(state, action);
        case actionTypes.LOGOUT_FAIL:
            return logoutFail(state, action);
        default:
            return state;
    }
}

export default reducer;