import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../../shared/utility';

const initialState = {
    messageList: [],
    messageAuditList:[],
    senderFilter: '',
    receiverFilter: '',
    nuPageAtual: 0,
    nuPageTotal: 0,
    error: null
}

const sendMessageSuccess = (state, action) => {
    return updateObject(state, {messageList: state.messageList.concat(action.message)});
}

const sendMessageFail = (state, action) => {
    return updateObject(state, {error: action.error});
}

const recoverConversationSuccess = (state, action) => {   
    return updateObject(state, {messageList: action.messageList});
}

const recoverConversationFail = (state, action) => {
    return updateObject(state, 
        {
            messageList: [],
            error: action.error
        });
}

const messagesAuditFindSuccess = (state, action) => {
    return updateObject(state, 
        {
            messageAuditList: action.messageAuditList, 
            nuPageAtual: action.nuPageAtual, 
            nuPageTotal: action.nuPageTotal
        });
}

const messagesAuditFindFail = (state, action) => {
    return updateObject(state, 
        {
            messageAuditList: [],
            nuPageAtual: 0, 
            nuPageTotal: 0,
            error: action.error
        });
}

const onChangeSenderFilter = (state, action) => {
    return updateObject(state, {senderFilter: action.sender});
}

const onChangeReceiverFilter = (state, action) => {
    return updateObject(state, {receiverFilter: action.receiver});
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SEND_MESSAGE_SUCCESS :
            return sendMessageSuccess(state, action);
        case actionTypes.SEND_MESSAGE_FAIL :
            return sendMessageFail(state, action);
        case actionTypes.RECOVER_CONVERSATION_SUCCESS:
            return recoverConversationSuccess(state, action);
        case actionTypes.RECOVER_CONVERSATION_FAIL:
            return recoverConversationFail(state, action);
        case actionTypes.MESSAGES_AUDIT_FIND_SUCCESS:
            return messagesAuditFindSuccess(state, action);
        case actionTypes.MESSAGES_AUDIT_FIND_FAIL:
            return messagesAuditFindFail(state, action);
        case actionTypes.CHANGE_SENDER_FILTER:
            return onChangeSenderFilter(state, action);
        case actionTypes.CHANGE_RECEIVER_FILTER:
            return onChangeReceiverFilter(state, action);
        default:
            return state;
    }
}

export default reducer;