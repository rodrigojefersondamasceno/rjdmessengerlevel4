import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    usersList: [],
    nuPageAtual: 0,
    nuPageTotal: 0,
    nameFilter: '',
    lastNameFilter: '',
    idSelectedUser: null,
    nameSelectedUser: null,
    error: null
}

const usersInit = (state) => {
    return updateObject(state, {
        error: null
    });
}

const usersFindSuccess = (state, action) => {
    return updateObject(state, {
        usersList: action.usersList,
        nuPageAtual: action.nuPageAtual,
        nuPageTotal: action.nuPageTotal,
        error: null
    });
}

const usersFindFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        usersList: [],
        nuPageAtual: 0,
        nuPageTotal: 0,
        idSelectedUser: null,
        nameSelectedUser: null,
    });
}

const selectUser = (state, action) => {
    return updateObject(state, { idSelectedUser: action.idSelectedUser, nameSelectedUser: action.nameSelectedUser});
}

const onChangeNameFilter = (state, action) => {
    return updateObject(state, {nameFilter: action.nameFilter});
}

const onChangeLastNameFilter = (state, action) => {
    return updateObject(state, {lastNameFilter: action.lastNameFilter});
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USERS_INIT:
            return usersInit(state);
        case actionTypes.USERS_FIND_SUCCESS:
            return usersFindSuccess(state, action);
        case actionTypes.USERS_FIND_FAIL:
            return usersFindFail(state, action);
        case actionTypes.SELECT_USER:
            return selectUser(state, action);
        case actionTypes.CHANGE_NAME_FILTER:
            return onChangeNameFilter(state, action);
        case actionTypes.CHANGE_LAST_NAME_FILTER:
            return onChangeLastNameFilter(state, action);
        default:
            return state;
    }
}

export default reducer;