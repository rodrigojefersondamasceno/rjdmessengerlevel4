import React from 'react';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';

import {Message} from '../../components/Messages/Message/Message';

configure({adapter: new Adapter()});

describe('Testing Message', () => {
    let wrapper;
    const dateStr = new Date().toLocaleString();
    beforeEach(() => {
        wrapper = shallow(<Message id='1' sender='fulano' receiver='beltrano' dtMessage={dateStr} message='mensagem 1' userLoged='fulano' resizeMessageAcordingToText={()=>{}}/>);
    });

    it("testing message header ", () => {
        expect(wrapper.contains(<p>fulano at {dateStr} to beltrano</p>)).toEqual(true);
    });

    it("testing message content ", () => {
        expect(wrapper.contains(<textarea ref="messageTextArea" id="messageTextArea" readOnly value='mensagem 1'/>)).toEqual(true);
    });
});