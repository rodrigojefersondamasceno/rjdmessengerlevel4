import React from 'react';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button from '../../components/UI/Button/Button';
import {MessageScreenFoot} from '../../components/MessageScreen/MessageScreenFoot/MessageScreenFoot';
import 'jest-enzyme';

configure({adapter: new Adapter()});

describe('Testing MessageScreenFoot', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<MessageScreenFoot 
                            changeTextArea={()=>{}}
                            valueTextArea=''
                            clickSendButton={()=>{}}/>);
    });

    it("testing button enabled", () => {
        expect(wrapper.find(Button)).not.toBeDisabled();
    });

    it("testing button disabled when props disabledSendButton == 'true' ", () => {
        wrapper.setProps({disabledSendButton: true});
        expect(wrapper.find(Button)).toBeDisabled();
    });
});