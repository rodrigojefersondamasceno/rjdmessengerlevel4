import React from 'react';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';

import {Messages} from '../../components/Messages/Messages';
import Message from '../../components/Messages/Message/Message';

configure({adapter: new Adapter()});

describe('Testing Messages', () => {
    let wrapper;
    const date = new Date();
    const dateStr = date.toLocaleString();
    const dateLongStr = date.getTime();
    const messageList = [
        {
            id:'1',
            message: 'mensagem 1',
            dtMessageLongStr: dateLongStr,
            senderName: 'fulano',
            receiverName: 'beltrano'
        },
        {
            id:'2',
            message: 'mensagem 2',
            dtMessageLongStr: dateLongStr,
            senderName: 'beltrano',
            receiverName: 'fulano'
        }
    ];

    beforeEach(() => {
        wrapper = shallow(<Messages messages={messageList} userLoged='fulano' scrollDownMessages={()=>{}}/>);
    });

    it("testing number of messages ", () => {
        expect(wrapper.find(Message).length).toEqual(2);
    });

    it("testing prop dtMessage of message 1 ", () => {
        expect(wrapper.find(Message).get(0).props.dtMessage).toEqual(dateStr);
    });

    it("testing prop dtMessage of message 1 ", () => {
        expect(wrapper.find(Message).get(0).props.message).toEqual('mensagem 1');
    });

    it("testing prop sender of message 1 ", () => {
        expect(wrapper.find(Message).get(0).props.sender).toEqual('fulano');
    });

    it("testing prop receiver of message 1 ", () => {
        expect(wrapper.find(Message).get(0).props.receiver).toEqual('beltrano');
    });
});