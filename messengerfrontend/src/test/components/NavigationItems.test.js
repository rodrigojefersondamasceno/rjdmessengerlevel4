import React from 'react';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';

import {NavigationItems} from '../../components/NavigationItems/NavigationItems';
import NavigationItem from '../../components/NavigationItems/NavigationItem/NavigationItem';

configure({adapter: new Adapter()});

describe('Testing NavigationIems', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<NavigationItems/>);
    });

    it("testing number NavigationItem whem not Authenticated ", () => {
        expect(wrapper.find(NavigationItem).length).toEqual(1);
    });

    it("testing presence NavigationItem '/login' whem not Authenticated ", () => {
        expect(wrapper.find(NavigationItem).get(0).props.link).toEqual('/login');
    });

    it("testing number NavigationItem whem Authenticated ", () => {
        wrapper.setProps({isAuthenticated: true});
        expect(wrapper.find(NavigationItem).length).toEqual(2);
    });

    it("testing presence NavigationItem '/main' whem Authenticated ", () => {
        wrapper.setProps({isAuthenticated: true});
        expect(wrapper.find(NavigationItem).get(0).props.link).toEqual('/main');
    });

    it("testing presence NavigationItem '/logout' whem Authenticated ", () => {
        wrapper.setProps({isAuthenticated: true});
        expect(wrapper.find(NavigationItem).get(1).props.link).toEqual('/logout');
    });
});