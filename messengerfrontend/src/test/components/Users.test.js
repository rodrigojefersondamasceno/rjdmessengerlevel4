import React from 'react';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';

import {Users} from '../../components/Users/Users';
import User from '../../components/Users/User/User';

configure({adapter: new Adapter()});

describe('Testing Users', () => {
    let wrapper;
    const userList = [
        {
            id:'fulanoSilva',
            name: 'Fulano',
            lastName: 'Silva'
        },
        {
            id:'beltranoSouza',
            name: 'Beltrano',
            lastNameName: 'Souza'
        }
    ];

    beforeEach(() => {
        wrapper = shallow(<Users usersList={userList} idSelectedUser='fulanoSilva'/>);
    });

    it("testing number of Users", () => {
        expect(wrapper.find(User).length).toEqual(2);
    });

    it("testing prop id of User", () => {
        expect(wrapper.find(User).get(0).props.id).toEqual('fulanoSilva');
    });

    it("testing prop name of User", () => {
        expect(wrapper.find(User).get(0).props.name).toEqual('Fulano');
    });

    it("testing prop lastName of User", () => {
        expect(wrapper.find(User).get(0).props.lastName).toEqual('Silva');
    });

    it("testing prop isSelected of User with id equal to idSelecetdUser", () => {
        expect(wrapper.find(User).get(0).props.isSelected).toEqual(true);
    });

    it("testing prop isSelected of User with id equal to idSelecetdUser", () => {
        expect(wrapper.find(User).get(1).props.isSelected).toEqual(false);
    });
});