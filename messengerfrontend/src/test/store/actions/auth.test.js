import * as actions from '../../../store/actions/auth';
import * as actionTypes from '../../../store/actions/actionTypes';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {spy} from 'sinon';
import axios from '../../../axios-instance';
import MockAdapter from 'axios-mock-adapter';

const middlewares = [thunk];

const mockStore = configureMockStore(middlewares);

const store = mockStore({userName: null,
            userId: null,
            userRole: null,
            isAuthenticated: false,
            error: null});

describe('auth action', () => {

    beforeEach(() => {
        store.clearActions();
    });

    it('when executing auth start', () => {
        expect(actions.authStart()).toEqual({type: actionTypes.AUTH_START});
    });

    it('when auth return success', () => {
        const mock = new MockAdapter(axios);
        const response = {
            data: {
                id: 'fulanoAdmin',
                name: 'Fulano ADMIN',
                userRole: 'ADMIN'
            }
        }
        const expectedActions = [
            {
              type: actionTypes.AUTH_SUCCESS,
              userId: response.data.id,
              userName: response.data.name,
              userRole: response.data.userRole
            },
          ];
        mock.onAny().reply(200,response);
        return store.dispatch(actions.auth('fulanoAdmin', 'fulano123')).then(() => {
            expect(store.getActions()).toEqual(expectedActions);

        });
    });
});