import reducer from '../../../store/reducers/auth';
import * as actionTypes from '../../../store/actions/actionTypes';

describe('auth reducer', () => {
    it('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            userName: null,
            userId: null,
            userRole: null,
            isAuthenticated: false,
            error: null
        })
    });

    it('should return state when actionType does not exist.', () => {
        expect(reducer({
            userName: 'Usuario Teste',
            userId: 'usrTeste',
            userRole: 'COMUM',
            isAuthenticated: true,
            error: null
        }, {
            type: 'Action não reconhecida'
        })).toEqual({
            userName: 'Usuario Teste',
            userId: 'usrTeste',
            userRole: 'COMUM',
            isAuthenticated: true,
            error: null
        })
    });

    it('should store data upon login successifull', () => {
        expect(reducer({
            userName: null,
            userId: null,
            userRole: null,
            isAuthenticated: false,
            error: null
        }, {
                type: actionTypes.AUTH_SUCCESS,
                userName: 'Usuario Teste',
                userId: 'usrTeste',
                userRole: 'COMUM',
            })).toEqual({
                userName: 'Usuario Teste',
                userId: 'usrTeste',
                userRole: 'COMUM',
                isAuthenticated: true,
                error: null
            })
    });

    it('should store error upon login fail', () => {
        expect(reducer({
            userName: 'Usuario Teste',
            userId: 'usrTeste',
            userRole: 'COMUM',
            isAuthenticated: true,
            error: null
        }, {
                type: actionTypes.AUTH_FAIL,
                error: 'a simple mistake'
            })).toEqual({
                userName: null,
                userId: null,
                userRole: null,
                isAuthenticated: false,
                error: 'a simple mistake'
            })
    });

    it('should erase data upon logout successfull', () => {
        expect(reducer({
            userName: 'Usuario Teste',
            userId: 'usrTeste',
            userRole: 'COMUM',
            isAuthenticated: true,
            error: null
        }, {
                type: actionTypes.LOGOUT_SUCCESS
            })).toEqual({
                userName: null,
                userId: null,
                userRole: null,
                isAuthenticated: false,
                error: null
            })
    });

    it('should save error on fail to logout', () => {
        expect(reducer({
            userName: 'Usuario Teste',
            userId: 'usrTeste',
            userRole: 'COMUM',
            isAuthenticated: true,
            error: null
        }, {
                type: actionTypes.LOGOUT_FAIL,
                error: 'a simple mistake'
            })).toEqual({
                userName: 'Usuario Teste',
                userId: 'usrTeste',
                userRole: 'COMUM',
                isAuthenticated: true,
                error: 'a simple mistake'
            })
    });
});