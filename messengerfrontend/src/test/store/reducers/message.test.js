import reducer from '../../../store/reducers/message';
import * as actionTypes from '../../../store/actions/actionTypes';

describe('message reducer', () => {
    const messageTest3 = {
        id:'3',
        message: 'mensagem 3',
        dtMessageLongStr: '1547480212398',
        senderName: 'fulano',
        receiverName: 'beltrano'
    };
    const messageListTest2 = [{
        id:'1',
        message: 'mensagem 1',
        dtMessageLongStr: '1547479823285',
        senderName: 'fulano',
        receiverName: 'beltrano'
    },{
        id:'2',
        message: 'mensagem 2',
        dtMessageLongStr: '1547480058082',
        senderName: 'beltrano',
        receiverName: 'fulano'
    }]

    it('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            messageList: [],
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        })
    });

    it('should insert message into messageList', () => {
        expect(reducer({
            messageList: messageListTest2,
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.SEND_MESSAGE_SUCCESS,
            message: messageTest3
        })).toEqual({
            messageList: [...messageListTest2,messageTest3],
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        })
    });

    it('should save error when send message fail.', () => {
        expect(reducer({
            messageList: messageListTest2,
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.SEND_MESSAGE_FAIL,
            error: 'a simple mistake'
        })).toEqual({
            messageList: messageListTest2,
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: 'a simple mistake'
        })
    });

    it('should recover messageList', () => {
        expect(reducer({
            messageList: null,
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.RECOVER_CONVERSATION_SUCCESS,
            messageList: messageListTest2,
        })).toEqual({
            messageList: messageListTest2,
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        })
    });

    it('should clean messageList when recover_conversation fail.', () => {
        expect(reducer({
            messageList: messageListTest2,
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.RECOVER_CONVERSATION_FAIL,
            error: 'um erro qualquer'
        })).toEqual({
            messageList: [],
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: 'um erro qualquer'
        })
    });

    it('should store messageAuditList, nuPageAtual and nuPageTotal', () => {
        expect(reducer({
            messageList: [],
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.MESSAGES_AUDIT_FIND_SUCCESS,
            messageAuditList: messageListTest2,
            nuPageAtual: 1,
            nuPageTotal: 1
        })).toEqual({
            messageList: [],
            messageAuditList: messageListTest2,
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 1,
            nuPageTotal: 1,
            error: null
        })
    });

    it('should clean messageAuditList, nuPageAtual and nuPageTotal when MESSAGES_AUDIT_FIND Fail', () => {
        expect(reducer({
            messageList: [],
            messageAuditList:messageListTest2,
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 1,
            nuPageTotal: 1,
            error: null
        }, {
            type: actionTypes.MESSAGES_AUDIT_FIND_FAIL,
            error: 'um erro qualquer'
        })).toEqual({
            messageList: [],
            messageAuditList: [],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: 'um erro qualquer'
        })
    });

    it('should store senderFilter', () => {
        expect(reducer({
            messageList: [],
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.CHANGE_SENDER_FILTER,
            sender: 'maria'
        })).toEqual({
            messageList: [],
            messageAuditList: [],
            senderFilter: 'maria',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        })
    });

    it('should store receiverFilter', () => {
        expect(reducer({
            messageList: [],
            messageAuditList:[],
            senderFilter: '',
            receiverFilter: '',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        }, {
            type: actionTypes.CHANGE_RECEIVER_FILTER,
            receiver: 'maria'
        })).toEqual({
            messageList: [],
            messageAuditList: [],
            senderFilter: '',
            receiverFilter: 'maria',
            nuPageAtual: 0,
            nuPageTotal: 0,
            error: null
        })
    });
});