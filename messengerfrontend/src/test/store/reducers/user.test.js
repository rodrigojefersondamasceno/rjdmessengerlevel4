import reducer from '../../../store/reducers/user';
import * as actionTypes from '../../../store/actions/actionTypes';

describe('message reducer', () => {

    const userList = [
        {
            id: 'fulanoSilva',
            name: 'Fulano',
            lastName: 'Silva'
        },
        {
            id: 'beltranoSouza',
            name: 'Beltrano',
            lastNameName: 'Souza'
        }
    ];

    it('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            usersList: [],
            nuPageAtual: 0,
            nuPageTotal: 0,
            nameFilter: '',
            lastNameFilter: '',
            idSelectedUser: null,
            nameSelectedUser: null,
            error: null
        })
    });

    it('should populate userList', () => {
        expect(reducer({
            usersList: [],
            nuPageAtual: 0,
            nuPageTotal: 0,
            nameFilter: '',
            lastNameFilter: '',
            idSelectedUser: 'mariaJose',
            nameSelectedUser: 'Maria José',
            error: null
        }, {
                type: actionTypes.USERS_FIND_SUCCESS,
                usersList: userList,
                nuPageAtual: 1,
                nuPageTotal: 1,
            })).toEqual({
                usersList: userList,
                nuPageAtual: 1,
                nuPageTotal: 1,
                nameFilter: '',
                lastNameFilter: '',
                idSelectedUser: 'mariaJose',
                nameSelectedUser: 'Maria José',
                error: null
            })
    });

    it('should save error when users find fail.', () => {
        expect(reducer({
            usersList: [],
            nuPageAtual: 0,
            nuPageTotal: 0,
            nameFilter: '',
            lastNameFilter: '',
            idSelectedUser: null,
            nameSelectedUser: null,
            error: null
        }, {
                type: actionTypes.USERS_FIND_FAIL,
                error: 'a simple mistake'
            })).toEqual({
                usersList: [],
                nuPageAtual: 0,
                nuPageTotal: 0,
                nameFilter: '',
                lastNameFilter: '',
                idSelectedUser: null,
                nameSelectedUser: null,
                error: 'a simple mistake'
            })
    });

    it('should save selectedUser.', () => {
        expect(reducer({
            usersList: userList,
            nuPageAtual: 1,
            nuPageTotal: 1,
            nameFilter: '',
            lastNameFilter: '',
            idSelectedUser: null,
            nameSelectedUser: null,
            error: null
        }, {
                type: actionTypes.SELECT_USER,
                idSelectedUser: 'userId',
                nameSelectedUser: 'userName',
            })).toEqual({
                usersList: userList,
                nuPageAtual: 1,
                nuPageTotal: 1,
                nameFilter: '',
                lastNameFilter: '',
                idSelectedUser: 'userId',
                nameSelectedUser: 'userName',
                error: null
            })
    });
})